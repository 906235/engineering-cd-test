#!/usr/bin/env python

""" This module is used create the stack in the AWS account """

import time
import json
import sys
import socket
import re
import argparse
import logging
import os.path
import boto.cloudformation
import boto.ec2.elb
import boto.route53
import boto.s3
import boto.sns

from boto.s3.key import Key
from connections.awsconn import AwsBotoConnections
from autoscaling.autoscaling import check_autoscaling
from libs.log import module_logger, setup_logging
from common.common import route53_zone, route53_get_cname_string

# Logging
LOG = module_logger(__name__)
setup_logging(logging.INFO)


# Exception class
class VantamException(Exception):
    """ Exception class """
    pass

# Global variables (these should really be in a config file)
_DEBUG_S3_BUCKET = "sni-vantam"


def arg_parse_module(args):
    """ Function to Read JSON Parameters File"""
    LOG.info('Getting Arguments')
    parser = argparse.ArgumentParser(description='CD Stack Creation')
    parser.add_argument('--params',
                        required=True,
                        help='JSON Parameters file.')
    parser.add_argument('--template',
                        required=False,
                        help="Alternative CloudFormation template than the default. " \
                             "Either specify the s3://bucket/file.template or file://file.template to replace.")
    parser.add_argument('--version',
                        required=True,
                        help='Application Version x.x.x')
    parser.add_argument('--sumosources',
                        required=False,
                        help="Alternate sumosources.json file to override system default one. " \
                             "If none specified the sumosources-ENV.json in deploy directory gets picked up if present")
    parser.add_argument('--logrotate',
                        required=False,
                        help="To specify additional application logs for logrotate to manage. " \
                             "If none specified the logrotate-ENV in deploy directory gets picked up if present")
    parser.add_argument('--disable-ip-transform', '-p',
                        required=False,
                        action='store_true',
                        help='Disable the IP 10.x.x.x to 11.x.x.x transform required by Cloudbees VPN.')

    args = parser.parse_args(args)
    return args


def get_avail_zones_from_subnet(conn_vpc, data):
    """ Function that will return the availability zone associated to the subnets.
        :param conn_vpc: The Boto VPC connection object
        :param data: The application configuration parameters
        :type conn_vpc: Object
        :type data: Dictionary
        :returns: the list of availability zones and the number of subnets

    """
    try:
        subnets = [v for k, v in data.iteritems() if k.startswith('Subnet')]
        availability_zones = [subnet.availability_zone for subnet in conn_vpc.get_all_subnets(subnets)]
        return availability_zones, len(subnets)
    except boto.exception.BotoServerError as error:
        LOG.error(error)
        return None


def set_platform_parameters(data, version):
    """ Function to set platform related parameters.
        :param data: The application configuration parameters
        :param version: The application version
        :type data: Dictionary
        :type version: String
        :returns: the updated parameters (data)
    """
    if 'Healthcheck' in data:
        try:
            port, health_uri = re.search(r'.*?:(.*?)(\/.*)', data['Healthcheck']).groups()
        except Exception, error:
            LOG.error("""Problems extracting the service_port and health_uri values from the "Healthcheck" parameter""")
            raise VantamException(error)
    else:
        raise VantamException("""Missing "Healthcheck" parameter from application parameters file!""")

    data["app_version"] = version
    data["AppVersion"] = version
    if data["Platform"] == 'jboss6':
        try:
            data['classes'].index('jboss6_deploy')
        except ValueError:
            data['classes'].append('jboss6_deploy')
        data["jboss6_deploy::app_version"] = version
        data["jboss6_deploy::service_port"] = port
        data["jboss6_deploy::health_uri"] = health_uri
        data["jboss6_deploy::conf_env"] = data['TagEnv']
        if 'jboss6_deploy::app_deploy_name' not in data:
            data["jboss6_deploy::app_deploy_name"] = data["AppName"]
    elif data["Platform"] == 'dropwizard':
        try:
            data['classes'].index('drop_wizard_deploy')
        except ValueError:
            data['classes'].append('drop_wizard_deploy')
        data["drop_wizard_deploy::app_version"] = version
        data["drop_wizard_deploy::service_port"] = port
        data["drop_wizard_deploy::health_uri"] = health_uri
        data["drop_wizard_deploy::conf_env"] = data['TagEnv']
        if 'drop_wizard_deploy::run_command' in data:
            LOG.info('drop_wizard_deploy::run_command already set')
        else:
            data["drop_wizard_deploy::run_command"] = "server"
            LOG.info('Setting drop_wizard_deploy::run_command to server...')
        if 'drop_wizard_deploy::app_deploy_name' not in data:
            data["drop_wizard_deploy::app_deploy_name"] = data["AppName"]
    elif data["Platform"] == 'angular':
        try:
            data['classes'].index('apache_app_deploy')
        except ValueError:
            data['classes'].append('apache_app_deploy')
        data["apache_app_deploy::app_version"] = version
        data["apache_app_deploy::service_port"] = port
        data["apache_app_deploy::health_uri"] = health_uri
        data["apache_app_deploy::conf_env"] = data['TagEnv']
        if 'apache_app_deploy::app_deploy_name' not in data:
            data["apache_app_deploy::app_deploy_name"] = data["AppName"]
    elif data["Platform"] == 'tomcat':
        try:
            data['classes'].index('tomcat_deploy')
        except ValueError:
            data['classes'].append('tomcat_deploy')
        data["tomcat_deploy::app_version"] = version
        data["tomcat_deploy::service_port"] = port
        data["tomcat_deploy::health_uri"] = health_uri
        data["tomcat_deploy::conf_env"] = data['TagEnv']
        if 'tomcat_deploy::app_deploy_name' not in data:
            data["tomcat_deploy::app_deploy_name"] = data["AppName"]
    elif data["Platform"] == 'springboot':
        try:
            data['classes'].index('springboot_deploy')
        except ValueError:
            data['classes'].append('springboot_deploy')
        data["springboot_deploy::app_version"] = version
        data["springboot_deploy::service_port"] = port
        data["springboot_deploy::health_uri"] = health_uri
        data["springboot_deploy::conf_env"] = data['TagEnv']
        if 'springboot_deploy::app_deploy_name' not in data:
            data["springboot_deploy::app_deploy_name"] = data["AppName"]
    else:
        raise VantamException('ERROR: Platform not supported. ' \
                              'Must be jboss6, dropwizard, tomcat, springboot, or angular.')

    return data


def set_notification_parameters(data):
    """ Function to set Business Unit Email & Alternate Business Unit Email parameters used for SNS Notifications.
        :param data: The application configuration parameters
        :type data: Dictionary
        :returns: the updated parameters (data)
    """
    cd_regex = re.compile(r'^(content.*delivery)$')
    media_regex = re.compile(r'^media$')
    both_regex = re.compile(r'^mcd$')

    if cd_regex.match(data["TagBusUnit"].lower()):
        data["BusinessUnitEmail"] = "dl-contentdeliveryengineering@scrippsnetworks.com"
        data["AlternateBusinessUnitEmail"] = ""
    elif media_regex.match(data["TagBusUnit"].lower()):
        data["BusinessUnitEmail"] = "dl-mediaengineering@scrippsnetworks.com"
        data["AlternateBusinessUnitEmail"] = ""
    elif both_regex.match(data["TagBusUnit"].lower()):
        data["BusinessUnitEmail"] = "dl-contentdeliveryengineering@scrippsnetworks.com"
        data["AlternateBusinessUnitEmail"] = "dl-mediaengineering@scrippsnetworks.com"
    else:
        raise VantamException('ERROR: BusinessUnit not supported,' \
                              'must be either: contentdelivery|content-delivery|media|mcd')

    return data


def compile_params(conn_vpc, app_params, system_params_file_path, args):
    """ Function to gather all the params values (application & system params) and build out the json object
        for the cloudformation stack creation. Default values can now be set withing the system params files since
        necessary settings can be enforced there.
        :param conn_vpc: The Boto VPC connection object
        :param app_params: The application configuration parameters
        :param args: The arguments object
        :type conn_vpc: Object
        :type app_params: Dictionary
        :type args: Object
        :returns: the updated parameters (data)
    """

    # get system parameters
    system_params_file = os.path.join(os.path.abspath(os.path.curdir), system_params_file_path, \
                                      "params-system-%s.json" % (app_params['TagEnv']))

    LOG.info("Loading system parameters %s...", system_params_file)
    tmp_params = json.load(open(system_params_file))
    system_params = dict()
    for k in ['AMImage', 'InstanceSize', 'classes']:
        system_params[k] = tmp_params[k]

    if "AccountRoleArn" in app_params:
        account = app_params['AccountRoleArn'].split(':')[4]
    else:
        account = 'default'

    if account not in tmp_params['Account']:
        raise VantamException('ERROR: No system account parameters available')

    system_params.update(tmp_params['Account'][account].items())
    system_params.update(app_params)
    data = dict(system_params)

    # a few one-off defaults with a bit of logic involved
    LOG.info("Adding cloudwatch parameters...")
    if "cloudwatch_logs::log_location" in data:
        data['cloudwatch_logs::app_name'] = "%s-%s" % (data['AppName'], data['TagEnv'])

    # add availability zone values to the data
    # because AZ data is tied to subnets, it's better to use the
    # subnet values to determine which are the appropriate zones
    # rather than explicitly assign them running the (continued)
    # risk of a conflict when spinning the cf stack.
    LOG.info("Assigning availability zone values from the subnet data...")
    zones, nb_subnet = get_avail_zones_from_subnet(conn_vpc, data)
    if len(zones) == nb_subnet:
        data['AvailZone1'] = zones[0]
        data['AvailZone2'] = zones[1]
    else:
        LOG.info("Error retrieving AvailabilityZone corresponding to subnets")
        sys.exit(1)

    # Add Mandatory New Relic parameters
    LOG.info('Setting new relic in parameters file...')
    try:
        data['classes'].index('newrelic')
    except ValueError:
        data['classes'].append('newrelic')
    data["newrelic::app_name"] = data["AppName"]
    data["newrelic::env"] = data["TagEnv"]

    # Add Mandatory sumo_deploy parameters
    try:
        data['classes'].index('sumo_deploy')
    except ValueError:
        data['classes'].append('sumo_deploy')
    data["sumo_deploy::business_unit"] = data["TagBusUnit"]
    data["sumo_deploy::dev_team"] = data["TagOwner"]
    data["sumo_deploy::sumo_env"] = data["TagEnv"]
    data["sumo_deploy::container_type"] = data["Platform"]
    data["sumo_deploy::app_name"] = data["AppName"]

    # Remove loggly
    try:
        index = data['classes'].index('loggly')
        del data['classes'][index]
    except ValueError:
        pass

    # Set Platform & Notification parameters
    LOG.info('Setting platform & Notification parameters...')
    data = set_platform_parameters(data, args.version)
    data = set_notification_parameters(data)

    # Write out the updated params file
    LOG.info('Outputting updated parameters file...')
    with open(args.params, 'w') as outfile:
        json.dump(data, outfile, sort_keys=True, indent=4, ensure_ascii=False)

    return data


def s3_upload(conn_s3, data, args):
    """Function to upload params file to S3.
       :param conn_s3: The Boto S3 connection object
       :param data: The configuration parameters
       :param args: The arguments object
       :type conn_s3: Object
       :type data: Dictionary
       :type args: Object
    """
    LOG.info('Uploading Params File To S3')
    bucket = conn_s3.get_bucket('sni-puppet')
    k = Key(bucket)
    k.key = "%s/%s/params-%s-%s.json" % (data["TagEnv"], data["AppName"], data["TagEnv"], data["AppVersion"])
    k.set_contents_from_filename(args.params)

    sumo_alternate_source = None
    sumosource_in_deploy = "../deploy/sumosources-%s.json" % (data["TagEnv"])
    if args.sumosources is not None:
        sumo_alternate_source = args.sumosources
        LOG.info('Alternate sumosources specified on command line')
    elif os.path.exists(sumosource_in_deploy):
        sumo_alternate_source = sumosource_in_deploy
        LOG.info('Using sumosources found in deploy directory')

    if sumo_alternate_source is not None:
        LOG.info('Uploading sumosources File To S3')
        k.key = "%s/%s/sumosources-%s-%s.json" % (data["TagEnv"], data["AppName"], data["TagEnv"], data["AppVersion"])
        k.set_contents_from_filename(sumo_alternate_source)

    app_logrotate = None
    logrotate_in_deploy = "../deploy/logrotate-%s" % (data["TagEnv"])
    if args.logrotate is not None:
        app_logrotate = args.logrotate
        LOG.info('logrotate file specified on command line')
    elif os.path.exists(logrotate_in_deploy):
        app_logrotate = logrotate_in_deploy
        LOG.info('logrotate file found in deploy directory')

    if app_logrotate is not None:
        LOG.info('Uploading logrotate File To S3')
        k.key = "%s/%s/app_logrotate-%s-%s" % (data["TagEnv"], data["AppName"], data["TagEnv"], data["AppVersion"])
        k.set_contents_from_filename(app_logrotate)


def route53_check(conn_53, data):
    """Function to check if the application cname is already created, if not add it to the zone domain.
       :param conn_53: The Boto Route53 connection object
       :param data: The configuration parameters
       :type conn_53: Object
       :type data: Dictionary
    """
    LOG.info('Route53 Check')
    zone_string = route53_zone(data)
    cname_string = route53_get_cname_string(data, zone_string)

    if cname_string is None:
        LOG.info('ERROR: Route53_check environment zone lookup error.')
        sys.exit(1)

    zone = conn_53.get_zone(zone_string[:-1])
    zone_results = zone.get_cname(cname_string)
    if zone_results is None:
        LOG.info('No Route53 CNAME. Adding........')
        zone.add_cname(cname_string, 'nothing.scrippsnetworks.com', ttl=60, comment='Added by CD')
    else:
        LOG.info('Route53 CNAME already in place.')


def stack_check(conn, conn_53, conn_elb, data, template=None):
    """Function to check how many stacks are already running and take action based on it.
       :param conn: The Boto CloudFormation connection object
       :param conn_53: The Boto Route53 connection object
       :param conn_elb: The Boto ELS connection object
       :param data: The configuration parameters
       :param template: The Cloud Formation template path to use when creating the stack
       :type conn: Object
       :type conn_53: Object
       :type conn_elb: Object
       :type data: Dictionary
       :type template: String
    """
    LOG.info('Checking if any other stacks exist for this application.')

    try:
        conn.describe_stacks(data["StackName"] + '-1')
        stack_1_status = True
        LOG.info('Stack 1 DOES exist.')
    except boto.exception.BotoServerError:
        LOG.info('Stack 1 does NOT exist.')
        stack_1_status = False

    try:
        conn.describe_stacks(data["StackName"] + '-2')
        stack_2_status = True
        LOG.info('Stack 2 DOES exist.')
    except boto.exception.BotoServerError:
        LOG.info('Stack 2 does NOT exist.')
        stack_2_status = False

    try:
        conn.describe_stacks(data["StackName"] + '-3')
        stack_3_status = True
        LOG.info('Stack 3 DOES exist.')
    except boto.exception.BotoServerError:
        LOG.info('Stack 3 does NOT exist.')
        stack_3_status = False

    if stack_1_status is False and stack_2_status is False and stack_3_status is False:
        data["StackName"] += '-1'
    elif stack_1_status is True and stack_2_status is True:
        stack_list = ['1', '2']
        LOG.info("Current Stacks 1 and 2")
        delete_stack_green = blue_green_stack(conn, conn_53, conn_elb, data, stack_list)
        delete_stack(conn, delete_stack_green)
        data["StackName"] += '-3'
    elif stack_1_status is True and stack_3_status is True:
        stack_list = ['1', '3']
        LOG.info("Current Stacks 1 and 3")
        delete_stack_green = blue_green_stack(conn, conn_53, conn_elb, data, stack_list)
        delete_stack(conn, delete_stack_green)
        data["StackName"] += '-2'
    elif stack_2_status is True and stack_3_status is True:
        stack_list = ['2', '3']
        LOG.info("Current Stacks 2 and 3")
        delete_stack_green = blue_green_stack(conn, conn_53, conn_elb, data, stack_list)
        delete_stack(conn, delete_stack_green)
        data["StackName"] += '-1'
    elif stack_1_status is True:
        blue_stack(conn_53, data)
        data["StackName"] += '-2'
    elif stack_2_status is True:
        blue_stack(conn_53, data)
        data["StackName"] += '-3'
    else:
        blue_stack(conn_53, data)
        data["StackName"] += '-1'

    create_stack(conn, data, template)

    return data


def delete_stack(conn, delete_stack_green):
    """Function to delete a Cloud Form Stack.
       :param conn: The Boto CloudFormation connection object
       :param delete_stack_green: The name of the stack to delete
       :type conn: Object
       :type delete_stack_green: String
    """
    try:
        LOG.info('Deleting old stack...')
        conn.delete_stack(delete_stack_green)
    except boto.exception.BotoServerError:
        LOG.info("Error deleting stack, %s. Please troubelshoot.", delete_stack_green)
        sys.exit(1)


def get_elb_ids(conn, data, stack_list):
    """Function to retrieve ELB IDs of Stacks.
       :param conn: The Boto CloudFormation connection object
       :param data: The configuration parameters
       :param stack_list: The array of stack numbers
       :type conn: Object
       :type data: Dictionary
       :type stack_list: Array
       :returns: a list of ELB ID
    """
    # Place holder for ELB IDs
    if len(stack_list) == 2:
        stack_elbids = ['1', '2']
    else:
        stack_elbids = ['1']

    i = 0
    for num in stack_list:
        stacks = conn.describe_stacks(data["StackName"] + "-" + num)
        stack = stacks[0]
        try:
            # Get ELB ID
            response = stack.describe_resource('ElasticLoadBalancerApp')['DescribeStackResourceResponse']
            stack_elbids[i] = response['DescribeStackResourceResult']['StackResourceDetail']['PhysicalResourceId']
            LOG.info("Check GET ELB ID %s: %s", i, stack_elbids[i])
        except boto.exception.BotoServerError, error:
            print error.error_message
        i += 1
        time.sleep(5)
    return stack_elbids


def get_elb_dns(conn_elb, stack_elbids):
    """Function to retrieve ELB DNS Names with ELB IDs.
       :param conn_elb: The Boto ELB connection object
       :param stack_elbids: The array of ELB identifiers
       :type conn_elb: Object
       :type stack_elbids: Array
       :returns: a list of ELB DNS
    """
    stack_elbdns = ['1', '2']
    i = 0
    for elbid in stack_elbids:
        elbs = conn_elb.get_all_load_balancers(elbid)
        elb = elbs[0]
        stack_elbdns[i] = elb.dns_name
        LOG.info("Check GET ELB DNS %s: %s", i, stack_elbdns[i])
        i += 1
        time.sleep(5)
    return stack_elbdns


def blue_stack(conn_53, data):
    """Function to find current blue/live. This is if there is only one stack.
       :param conn_53: The Boto Route53 connection object
       :param data: The configuration parameters
       :type conn_53: Object
       :type data: Dictionary
    """
    # Look up blue stack and output DNS
    zone = route53_zone(data)
    get_zone = conn_53.get_zone(zone)
    cname_string = route53_get_cname_string(data, zone)
    cname = str(get_zone.get_cname(cname_string).resource_records[0])
    LOG.info("Blue DNS %s", cname)
    bg_print(blue=cname, green="Null")


def blue_green_stack(conn, conn_53, conn_elb, data, stack_list):
    """Function to find current blue/live and green/standby stack. This is if there is more than one stack.
       :param conn: The Boto CloudFormation connection object
       :param conn_53: The Boto Route53 connection object
       :param conn_elb: The Boto ELB connectin object
       :param data: The configuration parameters
       :param stack_list: The array of stack number
       :type conn: Object
       :type conn_53: Object
       :type conn_elb: Object
       :type data: Dictionary
       :type stack_list: Array
       :returns: the stack to delete
    """
    # Get ELB IDs
    real_blue_green = True
    stack_elbids = get_elb_ids(conn, data, stack_list)

    for elbid in stack_elbids:
        if elbid is '1':
            # Delete the stack that doesn't have an DNS (happens when previous stack creation failed)
            delete_stack_green = data["StackName"] + "-" + stack_list[0]
            real_blue_green = False
        if elbid is '2':
            # Delete the stack that doesn't have an DNS (happens when previous stack creation failed)
            delete_stack_green = data["StackName"] + "-" + stack_list[1]
            real_blue_green = False

    if real_blue_green:
        # Get ELB DNS
        stack_elbdns = get_elb_dns(conn_elb, stack_elbids)

        # Check CName to find blue stack and then know the green stack, then mark green for deletion
        time.sleep(5)
        zone = route53_zone(data)
        get_zone = conn_53.get_zone(zone)
        cname_string = route53_get_cname_string(data, zone)
        cname = str(get_zone.get_cname(cname_string).resource_records[0][:-1])

        if len(stack_elbids) == 2:
            if stack_elbdns[0] != cname:
                delete_stack_green = data["StackName"] + "-" + stack_list[0]
                blue = stack_elbdns[1]
            else:
                delete_stack_green = data["StackName"] + "-" + stack_list[1]
                blue = stack_elbdns[0]

        LOG.info("Blue DNS %s", blue)
        # Output blue/live stack
        bg_print(blue, green="Null")
    else:
        blue_stack(conn_53, data)

    LOG.info("Delete Stack %s", delete_stack_green)
    return delete_stack_green


def construct_create_stack_params(data):
    """Function to list of parameters to pass to Aws create_stack.
       :param data: The configuration parameters
       :type data: Dictionary
       :rtype list
       :returns the list of parameters
    """

    inclusions = ["Subnet1", "Subnet2", "AvailZone1", "AvailZone2", "Minsize", "Maxsize", "AppName", "TagEnv", \
                  "TagOwner", "TagBusUnit", "TagProject", "Snstopic", "IAMRole", "AMImage", "InstanceSize", \
                  "Securitygrp", "KeyName", "Elbsubnet1", "Elbsubnet2", "ElbPort", "ElbFrontPort", "ElbProto", \
                  "ConnTimeout", "CrossZone", "InstProto", "Sticky", "CertARN", "Healthcheck", "AppVersion"]

    base_parameters = [(k, v) for k, v in data.items() if k in inclusions]

    exclusions = ["CpuMonitorUpSecs", "CpuMonitorUpPeriods", "CpuMonitorDownSecs", "CpuMonitorDownPeriods"]
    as_parameters = [("AS%s" % k, v) for k, v in data["AutoScaling"].items() if k not in exclusions]
    if "CpuMonitorUpSecs" in data["AutoScaling"]:
        as_parameters.append(('ASCpuUpSecs', data["AutoScaling"]["CpuMonitorUpSecs"]))
    if "CpuMonitorUpPeriods" in data["AutoScaling"]:
        as_parameters.append(('ASCpuUpPeriods', data["AutoScaling"]["CpuMonitorUpPeriods"]))
    if "CpuMonitorDownSecs" in data["AutoScaling"]:
        as_parameters.append(('ASCpuDownSecs', data["AutoScaling"]["CpuMonitorDownSecs"]))
    if "CpuMonitorDownPeriods" in data["AutoScaling"]:
        as_parameters.append(('ASCpuDownPeriods', data["AutoScaling"]["CpuMonitorDownPeriods"]))

    return base_parameters + as_parameters


def create_stack(conn, data, template=None):
    """Function to create stack.
       :param conn: The Boto CloudFormation connection object
       :param data: The configuration parameters
       :param template: The Cloud Formation template path used to create the stack.
       :type conn: Object
       :type data: Dictionary
       :type template: String
    """
    LOG.info('Starting to create AWS Stack.')

    # Initialize the template parameters
    template_url = None
    template_body = None

    if not template or template.startswith('file'):
        # default template if none was provided
        template = 'file://cdCloudForm.template'
        file_re = re.compile(r'file://(.*)', re.X)

        if file_re.match(template):
            template = file_re.match(template).groups()[0]

        # read the local template file in as a string
        template_body_fp = open(template, 'r')
        template_body = template_body_fp.read()
        template_body_fp.close()

        LOG.info("Using template (body): %s", template)

    elif template.startswith('http'):
        template_url = template
        LOG.info("Using template url: %s", template_url)

    elif template.startswith('s3'):
        # s3 bucket reference can be in one of two ways.
        # we want to break down the s3:// reference into
        # the https format when it's provided like this.
        s3_re = re.compile(r's3://(.*?)/(.*)', re.X)

        if s3_re.match(template):
            template_url = "https://s3.amazonaws.com/%s/%s" % (s3_re.match(template).groups())
        LOG.info("Using template url: %s", template_url)


    try:
        conn.create_stack(data["StackName"], template_body=template_body, template_url=template_url,
                          parameters=construct_create_stack_params(data),
                          tags={'AppVersion': data["app_version"],
                                'Name': data["AppName"] + "-" + data["TagEnv"] + "-" + data["app_version"],
                                'BusinessUnit': data["TagBusUnit"],
                                'DNSName': data["DNSName"],
                                'Environment': data["TagEnv"],
                                'Project': data["TagProject"],
                                'Platform': data["Platform"],
                                'Owner': data["TagOwner"]},
                          notification_arns=[], disable_rollback=True, timeout_in_minutes=None, capabilities=None)
    except boto.exception.BotoServerError, error:
        print error.error_message
        sys.exit(1)


def change_health_check(elb_name, conn_elb):
    """ Function to set the health check for the provided elb_obj to something that will suspend
        respinning EC2 instances allowing developers to log into the instances to
        troubleshoot.
       :param conn_elb: The Boto ELB connection object
       :param elb_name: The ELB name.
       :type conn_elb: Object
       :type elb_name: String
    """
    try:
        LOG.info("Changing ELB health check to TCP:22...")
        LOG.info("Finding this load balancer: %s...", elb_name)
        elb1 = conn_elb.get_all_load_balancers([elb_name])[0]
        elb1.health_check.target = 'tcp:22'
        elb1.configure_health_check(elb1.health_check)
        LOG.info("Health check for %s changed to TCP:22 to allow for debugging.", (elb_name))
        return True
    except boto.exception.BotoServerError, error:
        LOG.info("Unable to change ELB health check to TCP:22. To debug, please change this manually from the console.")
        LOG.warning(error)
        return False


def stack_status(conn, data, conn_s3, conn_elb):
    """ Function to check the stack status.
        :param conn: The Boto Cloud Formation connection object
        :param conn_elb: The Boto ELB connection object
        :param conn_s3: The Boto S3 connection object
        :param data: The configuration parameters
        :type conn: Object
        :type conn_elb: Object
        :type conn_s3: Object
        :type data: Dictionary
    """

    def has_bucket_key(bucket, key_value):
        """check bucket for key value"""
        for k in bucket.get_all_keys():
            if k.name.startswith(key_value):
                return True
        return False

    try:
        status = str(conn.describe_stack_events(data["StackName"])[0])
        while status != 'StackEvent AWS::CloudFormation::Stack ' + data["StackName"] + ' CREATE_COMPLETE':
            status = str(conn.describe_stack_events(data["StackName"])[0])
            LOG.info(status)
            time.sleep(30)

            if status == 'StackEvent AWS::CloudFormation::Stack ' + data["StackName"] + ' CREATE_FAILED':
                # change the health check to something that won't keep failing and spinning up instances.
                # if the check fails, it's possible the process never got far enough to create the ELB.
                elb_name = [x for x in conn.list_stack_resources(data['StackName']) if "LoadBalancer" in x.resource_type]
                change_health_check(elb_name[0].physical_resource_id, conn_elb)

                # check to see if the puppet script has created the path in the vantam bucket and uploaded
                # the puppet and application log files. actually, we don't need to check for the files.
                # we'll just check for the existence of the path for the app in the bucket.
                wait_one = 60
                vantam_bucket = conn_s3.get_bucket(_DEBUG_S3_BUCKET)
                vantam_bucket_path = "%s/%s-%s/" % (data['TagEnv'].lower(), data['AppName'].lower(), data['app_version'])

                # let's give it a minute to show up...
                while not has_bucket_key(vantam_bucket, vantam_bucket_path) and wait_one:
                    time.sleep(5)
                    wait_one -= 5

                if has_bucket_key(vantam_bucket, vantam_bucket_path):
                    LOG.info("Check the log files for possible reasons for failure:")
                    LOG.info("  s3://%s/%s", vantam_bucket.name, vantam_bucket_path)
                else:
                    LOG.info("No path for the log files was created in the VanTam debug bucket. " \
                             "Please check the AWS CloudFormation events.")

                raise VantamException('Stack Creation Failed.')

    except boto.exception.BotoServerError, error:
        LOG.error(error)
        sys.exit(1)



def ip_transform(ec2_inst):
    """ Function to construct IP address.
        :param ec2_inst: The string
        :type ec2_inst: String
    """
    length = len(ec2_inst)
    ec2_inst = "11" + ec2_inst[2:length]
    return ec2_inst


def get_ip(dns):
    """ Function to return IP address of a domain. Just pass in the DNS..
        :param dns: The dns string
        :type dns: String
        :returns the ip address
    """
    LOG.info('Waiting for ELB DNS to resolve, so we can write IP to file.')
    # Set a timeout value just in case
    timeout_get_ip = time.time() + 60*8
    ip_status = False
    while ip_status is False:
        try:
            time.sleep(2)
            ip_address = socket.gethostbyname(dns)
            ip_status = True
        except socket.error:
            if time.time() > timeout_get_ip:
                LOG.info('ERROR: Cannot resolve ELB DNS. Exiting...')
                sys.exit(1)
    return ip_address


def bg_print(blue, green):
    """ Function to print blue and/or green ELB DNS to a file. Use for blue/green DNS switchover later.
        :param blue: The blue dns
        :param green: The green dns
        :type blue: String
        :type green: String
    """
    my_file = open("bg.properties", 'a')
    if blue != "Null":
        my_file.write("blue_dns = \"%s\"\n" % (blue))
    else:
        my_file.write("green_dns = \"%s\"\n" % (green))


def stack_results_print(ec2_inst, elb, i, disable_ip_transform):
    """ Function to write in a file the ELB URL and Acceptance HOST.
        :param ec2_inst: The EC2 instance identifier
        :param elb: The elb url
        :param i: Heu?
        :param disable_ip_transform: Flag disabling the IP transformation (CloudBee hack)
        :type ec2_inst: String
        :type elb: String
        :type i: String
        :type disable_ip_transform: Boolean
    """
    my_file = open("host.properties", 'a')

    if ec2_inst == 'none':
        my_file.write("ELB_URL = %s\n" % (elb))
        elb_ip = get_ip(elb)
        if not disable_ip_transform:
            elb_ip = ip_transform(elb_ip)
        my_file.write("ELB_IP = %s\n" % (elb_ip))
    else:
        if not disable_ip_transform:
            ec2_inst = ip_transform(ec2_inst)
        my_file.write("ACCEPTANCE_HOST%s = %s\n" % (i, ec2_inst))
    my_file.close()


def stack_results(conn, conn_elb, conn_ec2, data, disable_ip_transform=False):
    """ Function to get resource outputs, ELB and EC2 info from AWS Cloud Formation Stack.
        :param conn: The Boto Cloud Formation connection object
        :param conn_elb: The Boto ELB connection object
        :param conn_ec2: The Boto EC2 connection object
        :param data: The configuration parameters
        :param disable_ip_transform: Flag disabling the IP transformation (CloudBee hack)
        :type conn: Object
        :type conn_elb: Object
        :type conn_ec2: Object
        :type data: Dictionary
        :type disable_ip_transform: Boolean
    """

    # Get Cloud Form Stack
    stacks = conn.describe_stacks(data["StackName"])
    stack = stacks[0]
    # Get ELB ID
    response = stack.describe_resource('ElasticLoadBalancerApp')['DescribeStackResourceResponse']
    stack_elbid = response['DescribeStackResourceResult']['StackResourceDetail']['PhysicalResourceId']

    # Get EC2 Instances from ELB and ELB Info
    ec_elb = conn_elb.describe_instance_health(stack_elbid)
    elbs = conn_elb.get_all_load_balancers(stack_elbid)
    elb = elbs[0]
    LOG.info('ELB DNS URL:')
    LOG.info('http://' + elb.dns_name)
    stack_results_print('none', elb.dns_name, 'none', disable_ip_transform)
    # Output this new green elb dns stack
    bg_print(blue="Null", green=elb.dns_name)

    i = 1
    for instance in ec_elb:
        LOG.info('EC2 INSTANCE ' + str(i))
        LOG.info('EC2 Instance ID: ' + instance.instance_id)
        ec2_inst = conn_ec2.get_all_instances(instance_ids=instance.instance_id)
        LOG.info('EC2 Instance IP: ' + ec2_inst[0].instances[0].private_ip_address)
        stack_results_print(ec2_inst[0].instances[0].private_ip_address, 'none', i, disable_ip_transform)
        i = i + 1


def subscribe_to_topic(conn, data):
    """ Function to create Topic if doesn't exist, then subscribe all expected recipients to it.
        :param conn: The SNS Boto connection
        :param data: The configuration parameters
        :type conn: Object
        :type data: Dictionary
    """
    topic_name = data["StackName"]
    topic_arn = None

    try:
        # Get all Topics
        response = conn.get_all_topics()
        topics = response['ListTopicsResponse']['ListTopicsResult']['Topics']
        arns = [x['TopicArn'] for x in topics]
        names = [x.split(':')[5] for x in arns]

        if topic_name in names:
            topic_arn = arns[names.index(topic_name)]

        # Create the Topic
        if not topic_arn:
            response = conn.create_topic(topic_name)
            topic_arn = response['CreateTopicResponse']['CreateTopicResult']['TopicArn']
            LOG.info('Topic created: ' + topic_arn)

        # Verify Subscriptions for the Topic
        response = conn.get_all_subscriptions_by_topic(topic_arn)
        subscriptions = response['ListSubscriptionsByTopicResponse']['ListSubscriptionsByTopicResult']['Subscriptions']
        endpoints = [x['Endpoint'] for x in subscriptions]

        # Subscribe all recipients
        if data["TagOwner"] not in endpoints:
            conn.subscribe(topic_arn, 'email', data["TagOwner"])
            LOG.info('Topic : ' + topic_arn + ' add subscriber: ' + data["TagOwner"])
        if data["BusinessUnitEmail"] not in endpoints:
            conn.subscribe(topic_arn, 'email', data["BusinessUnitEmail"])
            LOG.info('Topic : ' + topic_arn + ' add subscriber: ' + data["BusinessUnitEmail"])
        if data["AlternateBusinessUnitEmail"] and data["AlternateBusinessUnitEmail"] not in endpoints:
            conn.subscribe(topic_arn, 'email', data["AlternateBusinessUnitEmail"])
            LOG.info('Topic : ' + topic_arn + ' add subscriber: ' + data["AlternateBusinessUnitEmail"])

        LOG.info('Topic used for this application: ' + topic_arn)
        data["Snstopic"] = topic_arn

    except boto.exception.BotoServerError, error:
        LOG.error(error)
        sys.exit(1)


def main():
    """Module entry point"""
    args = arg_parse_module(sys.argv[1:])
    json_data = open(args.params)
    data = json.load(json_data)

    connections = AwsBotoConnections(data)
    conn_vpc = connections.connection_vpc()

    LOG.info("Compiling the parameters for the deployment process...")
    data = compile_params(conn_vpc, data, "params", args)

    LOG.info('Checking for AutoScaling Setup')
    check_autoscaling(data)

    conn_s3 = connections.connection_s3()
    s3_upload(conn_s3, data, args)

    conn_53 = connections.connection_53()
    route53_check(conn_53, data)

    conn_sns = connections.connection_sns()
    subscribe_to_topic(conn_sns, data)

    template = args.template
    disable_ip_transform = args.disable_ip_transform

    conn = connections.connection()
    conn_elb = connections.connection_elb()
    data = stack_check(conn, conn_53, conn_elb, data, template)
    stack_status(conn, data, conn_s3, conn_elb)

    conn_ec2 = connections.connection_ec2()
    stack_results(conn, conn_elb, conn_ec2, data, disable_ip_transform)

if __name__ == '__main__':
    main()
