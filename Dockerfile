FROM python:2.7

RUN apt-get -y update && apt-get install -y \
  python \
  python-dev \
  python-pip


RUN pip install --upgrade pip
RUN pip install https://pypi.python.org/packages/2.7/b/boto/boto-2.39.0-py2.py3-none-any.whl
RUN pip install moto
RUN pip install astroid
RUN pip install https://pypi.python.org/packages/3.5/p/pylint/pylint-1.5.5-py2.py3-none-any.whl
RUN pip install httmock
RUN export PYTHONPATH=/Library/Python/2.7/site-packages/boto

