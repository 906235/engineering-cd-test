#!/usr/bin/env python

"""This module is used to provide functionality that can be share between different scripts."""

def is_not_blank(my_string):
    """Function to evaluate if a string is blank
       :param my_string: The string to evaluate
    """
    return my_string and my_string.strip()


def route53_zone(data):
    """Function to return the domain used depending of the environment
       :param data: The dictionary of config parameters
       :returns: the computed domain
    """
    if "Domain" in data and is_not_blank(data["Domain"]):
        domain = data["Domain"] + "."
        zone = domain
    else:
        domain = "snops.net."

        if data["TagEnv"] == 'prod':
            zone = "aws." + domain
        elif data["TagEnv"] == 'qa':
            zone = "awsqa." + domain
        else:
            zone = "awsdev." + domain

    return zone


def route53_get_cname_string(data, zone_string):
    """Function to return the cname of the application based on the environment
       :param data: The dictionary of config parameters
       :param zone_string: The domain returned by route53_zone function
       :returns: the computed cname
    """
    if data["TagEnv"] == 'dev':
        cname_string = data["DNSName"] + '-dev.' + zone_string
    elif data["TagEnv"] == 'int':
        cname_string = data["DNSName"] + '-int.' + zone_string
    elif data["TagEnv"] == 'qa':
        cname_string = data["DNSName"] + '.' + zone_string
    elif data["TagEnv"] == 'prod':
        cname_string = data["DNSName"] + '.' + zone_string
    else:
        cname_string = None

    return cname_string
