#!/usr/bin/env python
import sys
import time
import os
import argparse
import boto.cloudformation
import boto.ec2.elb
import boto.route53

sys.path.append("../")
from connections.awsconn import *

def arg_parse():
    parser = argparse.ArgumentParser(description='CD Green Stack Deletion')
    parser.add_argument('--env',
                        required=True,
                        help='prod, qa, dev')
    args = parser.parse_args()
    return args

def get_elb_id(stack):
    elbid = stack.describe_resource('ElasticLoadBalancerApp')['DescribeStackResourceResponse']['DescribeStackResourceResult']['StackResourceDetail']['PhysicalResourceId']
    return elbid

def get_elb_dns(elbid, conn_elb):
    elbs = conn_elb.get_all_load_balancers(elbid)
    elb = elbs[0]
    elb_dns = "%s." % (elb.dns_name)
    return elb_dns

def get_cname_app(stack):
    if stack.tags['Environment'] == 'dev':
        cname_app = "%s-dev.awsdev.snops.net" % (stack.tags['DNSName'])
        zone = 'awsdev.snops.net.'
    elif stack.tags['Environment'] == 'int':
        cname_app = "%s-int.awsdev.snops.net" % (stack.tags['DNSName'])
        zone = 'awsdev.snops.net.'
    elif stack.tags['Environment'] == 'qa':
        cname_app = "%s.awsqa.snops.net" % (stack.tags['DNSName'])
        zone = 'awsqa.snops.net.'
    else:
        cname_app = "%s.aws.snops.net" % (stack.tags['DNSName'])
        zone = 'aws.snops.net.'
    return cname_app, zone

def get_cname_live(conn_53, zone, cname_app):
    get_zone = conn_53.get_zone(zone)
    cname_live = str(get_zone.get_cname(cname_app).resource_records[0])
    return get_zone, cname_live

def main():
    args = arg_parse()
    aws_accounts = [args.env]
    stacks_live = []
    stacks_delete = []
    stacks_noncd = []
    for aws_account in aws_accounts:
        data = {'TagEnv': aws_account}
        conn = connection(data)
        conn_elb = connection_elb(data)
        conn_53 = connection_53(data)
        stacks = conn.describe_stacks()
        for stack in stacks:
            try:
                time.sleep(3)
                #Get ELBID
                elbid = get_elb_id(stack)
                #Get ELB DNS
                elb_dns = get_elb_dns(elbid, conn_elb)
                #Form Application CNAME and Route53 Zone
                cname_app, zone = get_cname_app(stack)
    
                #Get Live CNAME Value for Stack
                get_zone, cname_live = get_cname_live(conn_53, zone, cname_app)
    
                if cname_live == elb_dns:
                    #Stack is live, do NOT delete
                    stacks_live.append(stack.stack_name)
                else:
                    #Stack is not live, delete
                    stacks_delete.append(stack.stack_name)
                    conn.delete_stack(stack.stack_name)
                    #print cname_live
                    #print elb_dns
            except:
                #print 'Stack %s, not in CD.' % (stack.stack_name)
                stacks_noncd.append(stack.stack_name)
    
        print 'Blue/LIVE Stacks for %s: ' % (aws_account)
        for stack_live in stacks_live:
            print stack_live
    
        print '\n'
        print 'DELETED: Green/Standby Stacks for %s: ' % (aws_account)
        for stack_delete in stacks_delete:
            print stack_delete
    
        print '\n'
        print 'Non-CD Stacks %s: ' % (aws_account)
        for stack_noncd in stacks_noncd:
            print stack_noncd
        print '\n'
    
        #Empty lists for each run
        del stacks_live[:]
        del stacks_delete[:]
        del stacks_noncd[:]

if __name__ == '__main__':
    main()
