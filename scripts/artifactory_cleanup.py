#!/usr/bin/env python
import getopt
import json
import sys
import urllib2
from datetime import date
from datetime import timedelta
import os

class RequestWithMethod(urllib2.Request):
    """Workaround for DELETE and PUT requests with urllib2"""
    def __init__(self, url, method, data=None, headers={},
    origin_req_host=None, unverifiable=False):
        self._method = method
        urllib2.Request.__init__(self, url, data, headers,
        origin_req_host, unverifiable)

    def get_method(self):
        if self._method:
            return self._method
        else:
            return urllib2.Request.get_method(self)

def fetch_env(env):
    print "Checking if " + env + " is present in os.environ"
    try:
        os.environ[env]
    except:
        print "\t[Unset] " + env + " is not present thus we are expect it in the arg list"
        tmp="empty"
    else:
        print "\t[Set]   " + env + " found, but this may get overritten by the arg list"
        tmp=os.environ[env]
        
    return tmp


config = {
    'server': fetch_env('ARTIFACTORY_URL'),
    'repository': 'libs-releases-local',
    'dry_run': False,
    'username': fetch_env('ARTIFACTORY_USERNAME'),
    'password': fetch_env('ARTIFACTORY_PASSWORD'),
    'time_delay': 20,
    'verbose': False
}

def main():
    handle_args()
    init_urllib()

    # grab all folders tagged snapshotless in our group
    snapshotless_uris = query_artifactory(False)

    # grab all snapshotless marked as release candidates
    rc_uris = query_artifactory(True)

    # remove the release candidates from the list (so we don't delete them)
    deleteable_uris = list(set(snapshotless_uris) - set(rc_uris))

    for s in deleteable_uris:
        delete_artifact(to_storage_url(s))


def query_artifactory(release_candidates):
    # create the query url
    fullurl = config['server'] + 'api/search/aql'
    if config['verbose']:
        print 'Using query URL ' + fullurl

    # create the datestrign to add to query to find things
    # older than X days ago
    today = date.today()
    xdaysago = today - timedelta(days=config['time_delay'])
    timeQuery = xdaysago.isoformat() + "T00:00:00.00Z"

    # create the query body to post
    request_body = """items.find(
               {"$and": [
               {"repo":{"$eq":\"""" + config['repository'] + """\"}},
               {"path":{"$match":\"""" + config['group'] + """/*"}},
               {"type":{"$eq":"folder"}},
               {"@latest" : { "$nmatch" : "*" }},
               {"@snapshotless":{"$eq":"true"}},"""

    request_body += '{"@release-candidate":{"$eq":"true"}},' if release_candidates else ''

    request_body += """{"created" : {"$lt":\"""" + timeQuery + """\"}}
               ]})"""

    if config['verbose']:
        print request_body

    # post the query body to the url
    req = urllib2.Request(fullurl, request_body, {'Content-Type':
    'application/json'})
    response = urllib2.urlopen(req)
    response_content = response.read()
    response.close()

    if config['verbose']:
        print response_content

    # load the response as json
    data = json.loads(response_content)

    # create a list of paths to folders found by the query
    api_uris = []
    for entry in data['results']:
        api_uris.append(entry['path'] + "/" + entry['name'])

    return api_uris

def handle_args():
    opts, args = getopt.getopt(sys.argv[1:], 's:r:dg:t:u:p:v', 
        ['server=', 'repository=', 'dryrun', 'group=', 'time_delay=', 
        'username=', 'password=', 'verbose'])
    for opt, arg in opts:
        if opt in ('-s', '--server'):
            config['server'] = arg
        elif opt in ('-r', '--repository'):
            config['repository'] = arg
        elif opt in ('-d', '--dryrun'):
            config['dry_run'] = True
        elif opt in ('-u', '--username'):
            config['username'] = arg
        elif opt in ('-p', '--password'):
            config['password'] = arg
        elif opt in ('-g', '--group'):
            config['group'] = arg
        elif opt in ('-t', '--time_delay'):
            config['time_delay'] = int(arg)
        elif opt in ('-v', '--verbose'):
            config['verbose'] = True

    if not config['group']:
        print "The group parameter is required. Use  -g or --group to define."
        sys.exit(1)

    # Require the group to be at least three tokens
    if config['group'].count("/") < 1:
        print "Invalid group setting: " + config['group']
        print "Expected at least two slash delimited tokens. e.g. com/scrippsnetworks"
        sys.exit(1)
        
    if config['username'] == "empty":
        print "\nThe username parameter is required. Use  -u, --username or export ARTIFACTORY_USERNAME to define."
        sys.exit(1)
  
    if config['password'] == "empty":
        print "\nThe password parameter is required. Use  -p, --password or export ARTIFACTORY_PASSWORD to define."
        sys.exit(1)

    if config['server'] == "empty":
        print "\nThe server/url parameter is required. Use  -s, --server or export ARTIFACTORY_URL to define. (ie https://sni4bees.artifactoryonline.com/sni4bees)"
        sys.exit(1)



def init_urllib():
    passwdmgr = urllib2.HTTPPasswordMgrWithDefaultRealm()
    passwdmgr.add_password(None, config['server'],
        config['username'], config['password'])

    auth = urllib2.HTTPBasicAuthHandler(passwdmgr)
    opener = urllib2.build_opener(auth)
    urllib2.install_opener(opener)


def to_storage_url(artifact_path):
    return config['server'] + config['repository'] + "/" + artifact_path


def delete_artifact(url):
    if config['dry_run']:
        print 'DRY RUN -',

    print 'Deleting ' + url



if __name__ == '__main__':
    main()
