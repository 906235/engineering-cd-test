#!/usr/bin/env python
import time
import json
import sys
import argparse
import os
import requests
import logging


def informNewrelic(app_name, revision, env, nrkey):
    url = 'https://api.newrelic.com/deployments.xml'
    data = {
        "deployment[app_name]": app_name,
        "deployment[revision]": revision,
        'deployment[description]': "description added by " + sys.argv[0]
    }
    headers = {"x-api-key": nrkey}
    response = requests.post(url, headers=headers, data=data)
    try:
        response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        print "HTTPError:", e.message


def arg_parse():
    parser = argparse.ArgumentParser(description='Inform Newrelic of new deployment version')
    parser.add_argument('--params',
                        default=0,
                        help='Parameters file.')
    parser.add_argument('--version',
                        default=0,
                        help='Application version. Must be specified on command line')
    parser.add_argument('--nrkey',
                        default=0,
                        help='Newrelic API Key ( else os.environment[${ENV}_NEWRELIC_API_KEY] )')

    args = parser.parse_args()
    return args


def help():
    msg = """
----------------------------------------------------------------------
usage: informNewrelic.py [-h] [--params PARAMS] [--version VERSION]
                         [--nrkey NRKEY]

Inform Newrelic of new deployment version

optional arguments:
  -h, --help         Show this help message and exit
  --params PARAMS    Parameters file.
  --version VERSION  Application version. Must be specified on command line
  --nrkey NRKEY      Newrelic API key ( else os.environment[${ENV}_NEWRELIC_API_KEY] )
  """
    logging.exception("errors")
    print msg
    sys.exit(1)


def main():
    """Read in JSON Parameters File"""
    args = arg_parse()
    if args.params == 0:
        help()
        sys.exit(1)
    json_data = open(args.params)
    data = json.load(json_data)
    if args.nrkey == 0:
        try:
            args.nrkey = (os.environ[data['newrelic::env'].upper() + '_NEWRELIC_API_KEY'])
        except:
            args.nrkey = 0
    if args.params == 0 or args.nrkey == 0 or args.version == 0:
        help()

    if not args.nrkey:
        args.nrkey = os.environ[data['newrelic::env'].upper() + '_NEWRELIC_API_KEY']
    
    informNewrelic(
      data['newrelic::app_name'],
      args.version,
      data['newrelic::env'],
      args.nrkey
      )
#    print data['newrelic::app_name'] +
#    " " + data['newrelic::env'] + " " + version  + " " + nrkey


if __name__ == '__main__':
    main()
