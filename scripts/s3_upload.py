#!/usr/bin/env python
import os
from boto.s3.connection import S3Connection
from boto.s3.key import Key

def connection_s3():
    """AWS Connection"""
    conn_s3 = S3Connection(
        aws_access_key_id=os.environ['DEV_AWS_ACCESS_KEY_ID'],
        aws_secret_access_key=os.environ['DEV_AWS_SECRET_ACCESS_KEY'])
    return conn_s3

def s3_upload(conn_s3):
    bucket = conn_s3.get_bucket('sni-puppet-dev')
    key = Key(bucket)
    k = Key(bucket)
    k.key=os.environ['CF_TEMP']
    #k.set_contents_from_filename("../"+os.environ['CF_TEMP'])
    k.set_contents_from_filename(os.environ['CF_TEMP'])

def main():
    conn_s3 = connection_s3()
    s3_upload(conn_s3)

if __name__ == '__main__':
    main()
