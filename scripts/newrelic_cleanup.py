#!/usr/bin/python
#Clean up old servers in New Relic
#Uses New Relic API Python Interface
#Install: pip install newrelic-api
#http://new-relic-api.readthedocs.org/en/latest/index.html

from newrelic_api import *

def api_key_list():
  """List of the accounts API keys."""
  api_keys = ('ef1520679de9ac12288558a57de6a64bd5f38b3623be1fa','da9bb61e14e954dd6173b83ca9ec8c5aaafe9f72c1ea5e7','08a36171c7150d3e1016578b13aedb0df3024eadbc4abf1','6c0771f521b1a39869f818cb93ba400a54d413c7cde99db')
  return api_keys

def conn_servers(key):
  """Creates a Server Connection to New Relic API"""
  conn = Servers(api_key = key)
  return conn

def get_servers(conn):
  """Get all servers with the connection."""
  servers = conn.list()
  return servers

def main():
  """This program deletes old non-reporting servers in New Relic"""
  api_keys = api_key_list()
  for key in api_keys:
    conn = conn_servers(key)
    servers = get_servers(conn)
    for server in servers['servers']:
      if server['reporting'] == False:
        server_id = server['id']
        conn.delete(server_id)
        print server['name'] + " has been deleted."

if __name__ == '__main__':
    main()
