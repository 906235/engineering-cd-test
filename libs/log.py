"""
Logging utilities and objects.
"""
__author__ = "Timothy Weiand <timothy@tenmilesquare.com>"


import logging


class UnknownColorError(Exception):
    """ Unknown color. """
    pass


class UnknownColorTypeError(Exception):
    """ Unknown color type passed. """
    pass


def setup_logging(level=logging.DEBUG, color_type='none'):
    """
    Setup logging to go to stderr.

    Keyword Arguments:
        level -- Minimum logging level to display.
        color_type -- Type of color coding to use:
            'none'  - Use no color
            'light' - Color for light backgrounds.
            'dark'  - Color for dark backgrounds.
    """

    fmt = '%(asctime)s [%(levelname)-8s] ' + \
          '%(name)s:%(lineno)s:%(funcName)s - %(message)s'
    datefmt = '%m-%d %H:%M:%S'

    formatter = logging.Formatter(fmt=fmt, datefmt=datefmt)
    handler = ColorStreamHandler(color_type=color_type)
    handler.setFormatter(formatter)

    root = logging.getLogger()
    root.addHandler(handler)
    root.setLevel(level)
    return


def module_logger(mod_name):
    """ Create a logger to be used globally within a module. """

    mod_parts = mod_name.split('.')
    if len(mod_parts) > 1:
        mod_name = mod_parts[-1]

    return logging.getLogger(mod_name)


def ansi_color(fg_name, bg_name=None, bold=False):
    """
    Create ANSIColor.

    Available Colors:
        * black
        * blue
        * cyan
        * green
        * magenta
        * red
        * white
        * yellow

    Arguments:
        fg_name -- Forground color name or 'reset'.

    Keyword Arguments:
        bg_name -- Background color name (only set if not None).
        bold -- Make color bold.
    """
    retval = None

    color_map = {'black': '0',
                 'red': '1',
                 'green': '2',
                 'yellow': '3',
                 'blue': '4',
                 'magenta': '5',
                 'cyan': '6',
                 'white': '7'}

    if fg_name not in color_map and fg_name != 'reset':
        msg = 'Unknown foreground color requested \"{0}\"'
        raise UnknownColorError(msg.format(fg_name))

    if bg_name is not None and bg_name not in color_map:
        msg = 'Unknown background color requested \"{0}\"'
        raise UnknownColorError(msg.format(bg_name))

    if fg_name == 'reset':
        retval = '\x1b[0m'
    else:
        c_code = []

        if bg_name is not None:
            c_code.append('4' + color_map[bg_name])

        c_code.append('3' + color_map[fg_name])

        if bold:
            c_code.append('1')

        retval = '\x1b[' + ';'.join(c_code) + 'm'

    return retval


class ColorStreamHandler(logging.StreamHandler):
    """Color logging lines based on loglevel."""

    _use_color = True

    def __init__(self, color_type='none'):
        """
        Create ColorStreamHandler.

        Keyword Arguments:
            color_type -- Type of color coding to use:
                  'none'  - Use no color
                  'light' - Color for light backgrounds.
                  'dark'  - Color for dark backgrounds.
        """
        logging.StreamHandler.__init__(self)

        if color_type == 'none':
            self._use_color = False
        elif color_type == 'light':
            self.colors = {
                logging.DEBUG: ansi_color('black'),
                logging.INFO: ansi_color('green'),
                logging.WARNING: ansi_color('cyan'),
                logging.ERROR: ansi_color('red', bold=True),
                logging.CRITICAL: ansi_color('red', bold=True)}
        elif color_type == 'dark':
            self.colors = {
                logging.DEBUG: ansi_color('white'),
                logging.INFO: ansi_color('green'),
                logging.WARNING: ansi_color('yellow'),
                logging.ERROR: ansi_color('red', bold=True),
                logging.CRITICAL: ansi_color('red', bold=True)}
        else:
            msg = 'color_type value must be light, dark or none'
            raise UnknownColorTypeError(msg)

        self.reset = ansi_color('reset')

    def emit(self, record):
        """Emit a message record to stderr."""
        try:
            message = self.color_format(record)
            self.stream.write(message)
            self.stream.write(getattr(self, 'terminator', '\n'))
            self.flush()
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)

    def color_format(self, record):
        """Format message with color."""
        message = self.format(record)
        if self.use_color:
            message = '{0}{1}{2}'.format(self.colors[record.levelno],
                                         message,
                                         self.reset)
        return message

    @property
    def use_color(self):
        """Is color being used."""
        return self._use_color


class LoggingObject(object):
    """object with a loggger."""

    _log = None
    _log_name = None

    @property
    def log(self):
        """ Logger for this class. """
        return self._log

    @property
    def log_name(self):
        """ Log name for this class. """
        return self._log_name

    def __init__(self, log_name=None):
        """Create LoggingObject."""

        if log_name is None:
            self._log_name = self.__class__.__name__

        if self._log is None:
            self._log = logging.getLogger(self._log_name)

        return
