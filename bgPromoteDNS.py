#!/usr/bin/env python

""" This module is used to do a blue/green deployment (i.e switching DNS). """

import json
import argparse
import logging
from operator import attrgetter
import boto.cloudformation

from connections.awsconn import AwsBotoConnections
from libs.log import module_logger, setup_logging
from common.common import route53_zone, route53_get_cname_string

#Logging
LOG = module_logger(__name__)
setup_logging(logging.INFO)

def arg_parse():
    """ Function to Read JSON Parameters File"""
    LOG.info('Getting Module Arguments')
    parser = argparse.ArgumentParser(description='bgPromoteDNS')
    parser.add_argument('--params',
                        required=True,
                        help='JSON Parameters file.')

    args = parser.parse_args()
    return args

def read_file(filename):
    """ Function to read key=value from a file.
        :param filename: The file to read.
        :type filename: String
        :returns: a Dictionary containing the Blue and Green DNS(s).
        :rtype: Dictionary
    """
    my_file = open(filename, 'r')
    my_string = my_file.read()
    my_file.close()
    return dict((f.split('=', 1) for f in my_string.replace("\"", "").replace(" ", "").split("\n") if f.strip()))

def params_defaults(data):
    """ Function to initialize optional parameters.
        :param data: The configuration parameters
        :type data: Dictionary
    """
    if "DeleteOldStacks" in data:
        LOG.info('DeleteOldStacks set to %s', data["DeleteOldStacks"])
    else:
        if data["TagEnv"] == 'dev':
            data["DeleteOldStacks"] = "1"
            LOG.info('DeleteOldStacks: Set to default value. %s', data["DeleteOldStacks"])
        else:
            data["DeleteOldStacks"] = "0"
            LOG.info('DeleteOldStacks: Set to default value. %s', data["DeleteOldStacks"])

def blue_print(new_blue):
    """ Function to Print new blue to file.
        :param new_blue: The new blue dns address
        :type new_blue: String
    """
    LOG.info('********** DNS SWITCHED **********')
    LOG.info('Blue/Live DNS: %s', new_blue)
    my_file = open("bg.properties", 'w')
    my_file.write("blue_dns = \"%s\"\n" % (new_blue))
    my_file.close()

def blue_green_print(new_blue, new_green):
    """ Function to Print new blue & green DNS(s) to file.
        :param new_blue: The new blue dns
        :param new_green: The new green dns
        :type new_blue: String
        :type new_green: String
    """
    LOG.info('********** DNS SWITCHED **********')
    LOG.info('Blue/Live DNS: %s', new_blue)
    LOG.info('Green/Standby DNS: %s', new_green)
    my_file = open("bg.properties", 'w')
    my_file.write("blue_dns = \"%s\"\n" % (new_blue))
    my_file.write("green_dns = \"%s\"\n" % (new_green))
    my_file.close()

def kill_old_stacks(conn_cf, data):
    """ Function to Kill old stacks.
        :param conn_cf: Connection Object to AWS Cloud Formation
        :param data: The configuration parameters
        :type data: Dictionary
        :type conn_cf: Object
    """
    LOG.info('Killing old stacks')
    LOG.info('Getting All Live Stacks')
    active_stacks = conn_cf.list_stacks(stack_status_filters='CREATE_COMPLETE')

    app_stacks = [
        x for x in active_stacks if data["StackName"] in x.stack_name
    ]

    LOG.info('Removing newest stack from the list...')
    if app_stacks:
        keep = max(app_stacks, key=attrgetter('creation_time'))
        app_stacks.remove(keep)

        for app_stack in app_stacks:
            LOG.info('Stack Name To Delete: %s', app_stack.stack_name)
            try:
                LOG.info("Deleting old stack, %s.", app_stack.stack_name)
                conn_cf.delete_stack(app_stack.stack_id)
            except boto.exception.BotoServerError:
                LOG.info("Cannot delete stack, %s. Does not exist.", app_stack.stack_name)

def switch_dns(conn_53, conn_cf, data, zone, dns_data):
    """ Function to switch_dns.
        :param conn_53: Connection Object to AWS 53
        :param conn_cf: Connection Object to AWS Cloud Formation
        :param data: The configuration parameters
        :param zone: The zone domain to look for
        :param dns_data: The blue/green DNS(s) to switch
        :type conn_53: Object
        :type conn_cf: Object
        :type data: Dictionary
        :type zone: String
        :type dns_data: Dictionary
    """
    blue = bool("blue_dns" in dns_data)
    green = bool("green_dns" in dns_data)

    get_zone = conn_53.get_zone(zone)
    zone = route53_zone(data)
    cname_string = route53_get_cname_string(data, zone)

    if blue is False and green is True:
        get_zone.update_cname(cname_string, dns_data["green_dns"], ttl=60, comment='Added by CD')
        LOG.info('Application CNAME:')
        LOG.info('http://%s', cname_string[:-1])
        blue_print(dns_data["green_dns"])
        if data["DeleteOldStacks"] == '1':
            kill_old_stacks(conn_cf, data)

    elif blue is True and green is True:
        get_zone.update_cname(cname_string, dns_data["green_dns"], ttl=60, comment='Added by CD')
        LOG.info('Application CNAME:')
        LOG.info('http://%s', cname_string[:-1])
        if data["DeleteOldStacks"] == '1':
            kill_old_stacks(conn_cf, data)
            blue_print(dns_data["green_dns"])
        else:
            blue_green_print(dns_data["green_dns"], dns_data["blue_dns"])

    else:
        print "Error NO green to switch to blue."


def main():
    """Module entry point"""
    args = arg_parse()
    json_data = open(args.params)
    data = json.load(json_data)

    dns_data = read_file('bg.properties')

    params_defaults(data)
    connections = AwsBotoConnections(data)

    conn_53 = connections.connection_53()
    conn_cf = connections.connection()

    zone = route53_zone(data)
    LOG.info('Route53 Zone: %s', zone)
    switch_dns(conn_53, conn_cf, data, zone, dns_data)

if __name__ == '__main__':
    main()
