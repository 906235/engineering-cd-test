#!/usr/bin/env python

"""This module is used to establish connections with the different Boto AWS APIs."""

import os
import sys
import boto.cloudformation
import boto.ec2.elb
import boto.route53
import boto.sns
import boto.sts
import boto.vpc
from boto.s3.connection import S3Connection
from libs.log import module_logger

## Logging
LOG = module_logger(__name__)

class AwsBotoConnections(object):
    """ Class that encapsulate keys/credentials/connections used to call AWS BOTO APIs """

    def __init__(self, data):
        if "AccountRoleArn" in data:
            try:
                LOG.info('Retrieve credentials for the account role ' + data["AccountRoleArn"])
                # Connect first to AWS STS with dev keys credential
                conn_sts = boto.sts.connect_to_region('us-east-1',
                                                      aws_access_key_id=os.environ['DEV_AWS_ACCESS_KEY_ID'],
                                                      aws_secret_access_key=os.environ['DEV_AWS_SECRET_ACCESS_KEY'])

                # Switch Role and retrieve new credentials
                response = conn_sts.assume_role(data["AccountRoleArn"], "engineering-cd-access")
                self.access_key = response.credentials.access_key
                self.secret_key = response.credentials.secret_key
                self.session_token = response.credentials.session_token

            except boto.exception.BotoServerError as error:
                LOG.error(error)
                sys.exit(1)
        else:
            if (data["TagEnv"] == 'dev') or (data["TagEnv"] == 'int'):
                self.access_key = os.environ['DEV_AWS_ACCESS_KEY_ID']
                self.secret_key = os.environ['DEV_AWS_SECRET_ACCESS_KEY']
                self.session_token = None
            elif data["TagEnv"] == 'qa':
                self.access_key = os.environ['QA_AWS_ACCESS_KEY_ID']
                self.secret_key = os.environ['QA_AWS_SECRET_ACCESS_KEY']
                self.session_token = None
            elif data["TagEnv"] == 'prod':
                self.access_key = os.environ['PROD_AWS_ACCESS_KEY_ID']
                self.secret_key = os.environ['PROD_AWS_SECRET_ACCESS_KEY']
                self.session_token = None
            else:
                sys.exit(1)

        # Initialize all connections to None
        self.conn_cf = None
        self.conn_elb = None
        self.conn_ec2 = None
        self.conn_s3 = None
        self.conn_53 = None
        self.conn_sns = None
        self.conn_vpc = None

    def connection(self):
        """AWS Cloud Formation Connection"""
        if self.conn_cf is None:
            LOG.info('Making AWS Cloud Form Connection')
            try:
                self.conn_cf = boto.cloudformation.connect_to_region('us-east-1',
                                                                     aws_access_key_id=self.access_key,
                                                                     aws_secret_access_key=self.secret_key,
                                                                     security_token=self.session_token)
            except boto.exception.BotoServerError as error:
                LOG.error(error)
                sys.exit(1)
        return self.conn_cf

    def connection_elb(self):
        """AWS ELB Connection"""
        if self.conn_elb is None:
            LOG.info('Making AWS ELB Connection')
            try:
                self.conn_elb = boto.ec2.elb.connect_to_region('us-east-1',
                                                               aws_access_key_id=self.access_key,
                                                               aws_secret_access_key=self.secret_key,
                                                               security_token=self.session_token)
            except boto.exception.BotoServerError as error:
                LOG.error(error)
                sys.exit(1)
        return self.conn_elb

    def connection_ec2(self):
        """AWS EC2 Connection"""
        if self.conn_ec2 is None:
            LOG.info('Making AWS EC2 Connection')
            try:
                self.conn_ec2 = boto.ec2.connect_to_region('us-east-1',
                                                           aws_access_key_id=self.access_key,
                                                           aws_secret_access_key=self.secret_key,
                                                           security_token=self.session_token)
            except boto.exception.BotoServerError as error:
                LOG.error(error)
                sys.exit(1)
        return self.conn_ec2

    def connection_s3(self):
        """AWS S3 Connection"""
        if self.conn_s3 is None:
            LOG.info('Making AWS S3 Connection')
            try:
                self.conn_s3 = S3Connection(aws_access_key_id=self.access_key,
                                            aws_secret_access_key=self.secret_key,
                                            security_token=self.session_token)
            except boto.exception.BotoServerError as error:
                LOG.error(error)
                sys.exit(1)
        return self.conn_s3

    def connection_53(self):
        """AWS Route 53 Connection"""
        if self.conn_53 is None:
            LOG.info('Making AWS Route53 Connection')
            try:
                self.conn_53 = boto.route53.connect_to_region('us-east-1',
                                                              aws_access_key_id=self.access_key,
                                                              aws_secret_access_key=self.secret_key,
                                                              security_token=self.session_token)
            except boto.exception.BotoServerError as error:
                LOG.error(error)
                sys.exit(1)
        return self.conn_53

    def connection_sns(self):
        """AWS SNS Connection"""
        if self.conn_sns is None:
            LOG.info('Making AWS SNS Connection')
            try:
                self.conn_sns = boto.sns.connect_to_region('us-east-1',
                                                           aws_access_key_id=self.access_key,
                                                           aws_secret_access_key=self.secret_key,
                                                           security_token=self.session_token)
            except boto.exception.BotoServerError as error:
                LOG.error(error)
                sys.exit(1)
        return self.conn_sns

    def connection_vpc(self):
        """AWS VPC Connection"""
        if self.conn_vpc is None:
            LOG.info('Making AWS VPC Connection')
            try:
                self.conn_vpc = boto.vpc.connect_to_region('us-east-1',
                                                           aws_access_key_id=self.access_key,
                                                           aws_secret_access_key=self.secret_key,
                                                           security_token=self.session_token)
            except boto.exception.BotoServerError as error:
                LOG.error(error)
                sys.exit(1)
        return self.conn_vpc
