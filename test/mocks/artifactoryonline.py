"""
Mocks for the Artifactory API library tests.
"""

import os.path
from httmock import response, urlmatch


NETLOC = r'(.*\.)?sni4bees\.artifactoryonline\.com$'
HEADERS = {'content-type': 'application/json'}
PUT = 'put'
POST = 'post'
DELETE = 'delete'


class Resource:
    """ An Artifactory resource.
    :param path: The file path to the resource.
    """

    def __init__(self, path):
        self.path = path

    def get(self):
        """ Perform a GET request on the resource.
        :rtype: str
        """
        with open(self.path, 'r') as f:
            content = f.read()
        return content


@urlmatch(netloc=NETLOC, method=PUT)
def resource_put(url, request):
    try:
        url.query.index("properties")
        return response(201, "", HEADERS, None, 5, request)
    except IndexError:
        return response(404, {}, HEADERS, None, 5, request)


@urlmatch(netloc=NETLOC, method=DELETE)
def resource_delete(url, request):
    try:
        url.query.index("properties")
        return response(201, "", HEADERS, None, 5, request)
    except IndexError:
        return response(404, {}, HEADERS, None, 5, request)


@urlmatch(netloc=NETLOC, method=POST)
def resource_post(url, request):
    file_path = url.netloc + url.path
    response_file_name = os.path.join(os.path.abspath(os.path.curdir), "test", file_path)

    try:
        content = Resource(response_file_name).get()
    except EnvironmentError:
        # catch any environment errors (i.e. file does not exist) and return a 404
        return response(404, {}, HEADERS, None, 5, request)

    return response(200, content, HEADERS, None, 5, request)