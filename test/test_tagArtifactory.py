import unittest
import logging
import tagArtifactory
from libs.log import setup_logging
setup_logging(logging.ERROR)

from httmock import with_httmock
import mocks.artifactoryonline

class TestTagArtifactory(unittest.TestCase):

    def test_validate_json_file_content_missing_repo(self):
        params = {
                    "group" : "com/scrippsnetworks/engineering-jobdsltest",
                    "artifacts": [
                        "engineering-jobdsltest-service",
                        "engineering-jobdsltest-test",
                        "engineering-jobdsltest-api"
                    ]
                 }

        try:
            # Function under test
            tagArtifactory.validate_json_file_content(params)
        except tagArtifactory.VantamException:
            pass
        else:
            self.fail('Expecting VantamException')

    def test_validate_json_file_content_invalid_repo(self):
        params = {
            "repo": "invalid-releases-local",
            "group": "com/scrippsnetworks/engineering-jobdsltest",
            "artifacts": [
                "engineering-jobdsltest-service",
                "engineering-jobdsltest-test",
                "engineering-jobdsltest-api"
            ]
        }

        try:
            # Function under test
            tagArtifactory.validate_json_file_content(params)
        except tagArtifactory.VantamException:
            pass
        else:
            self.fail('Expecting VantamException')

    def test_validate_json_file_content_no_artifacts(self):
        params = {
            "repo": "libs-releases-local",
            "group": "com/scrippsnetworks/engineering-jobdsltest",
            "artifacts": []
        }

        try:
            # Function under test
            tagArtifactory.validate_json_file_content(params)
        except tagArtifactory.VantamException:
            pass
        else:
            self.fail('Expecting VantamException')

    def test_validate_json_file_content_no_group(self):
        params = {
            "repo": "libs-releases-local",
            "artifacts": [
                "engineering-jobdsltest-service",
                "engineering-jobdsltest-test",
                "engineering-jobdsltest-api"
            ]
        }

        try:
            # Function under test
            tagArtifactory.validate_json_file_content(params)
        except tagArtifactory.VantamException:
            pass
        else:
            self.fail('Expecting VantamException')

    def test_validate_json_file_content_well_formatted(self):
        params = {
                    "repo" : "libs-releases-local",
                    "group": "com/scrippsnetworks/engineering-jobdsltest",
                    "artifacts": [
                        "engineering-jobdsltest-service",
                        "engineering-jobdsltest-test",
                        "engineering-jobdsltest-api"
                    ]
                  }

        try:
            # Function under test
            tagArtifactory.validate_json_file_content(params)
        except tagArtifactory.VantamException:
            self.fail('Not Expecting VantamException')

    def test_stringify_properties_set(self):
        properties = { "snapshotless" : "true", "git_commit":"3f23aedb2f9e1fa49c783c272ee2ee00502e9904" }

        # Function under test
        result = tagArtifactory.stringify_properties_set(properties)

        # Verify
        self.assertEqual(result, 'snapshotless=true|git_commit=3f23aedb2f9e1fa49c783c272ee2ee00502e9904')

    def test_stringify_properties_delete(self):
        properties = {"snapshotless": "true", "git_commit": "3f23aedb2f9e1fa49c783c272ee2ee00502e9904"}

        # Function under test
        result = tagArtifactory.stringify_properties_delete(properties)

        # Verify
        self.assertEqual(result, 'snapshotless,git_commit')

    def test_stringify_properties_aql(self):
        properties = {"snapshotless": "true", "git_commit": "3f23aedb2f9e1fa49c783c272ee2ee00502e9904"}

        # Function under test
        result = tagArtifactory.stringify_properties_aql(properties)

        # Verify
        self.assertEqual(result, '{"@snapshotless" : "true"},{"@git_commit" : "3f23aedb2f9e1fa49c783c272ee2ee00502e9904"}')

    def test_get_base_storage_url_lib_repo(self):
        repo = "libs-releases-local"
        group = "com/scrippsnetworks/engineering-jobdsltest"
        version = "0.0.1.14"
        artifact = "engineering-jobdsltest-service"
        path = tagArtifactory.ArtifactoryPath("https://sni4bees.artifactoryonline.com/sni4bees")

        # Function under test
        result = tagArtifactory.get_base_storage_url(repo, group, version, artifact, path)

        # Verify
        expected_url = "https://sni4bees.artifactoryonline.com/sni4bees/api/storage/" + repo \
                       + '/' + group + '/' + artifact + '/' + version
        self.assertEqual(result,expected_url)

    def test_get_base_storage_url_npm_repo(self):
        repo = "npm-releases-local"
        group = "engineering-jobdsluitest/-"
        version = "0.0.1.14"
        artifact = "engineering-jobdsluitest"
        path = tagArtifactory.ArtifactoryPath("https://sni4bees.artifactoryonline.com/sni4bees")

        # Function under test
        result = tagArtifactory.get_base_storage_url(repo, group, version, artifact, path)

        # Verify
        expected_url = "https://sni4bees.artifactoryonline.com/sni4bees/api/storage/" + repo \
                       + '/' + group + '/' + artifact + '-' + version + '.tgz'
        self.assertEqual(result, expected_url)

    @with_httmock(mocks.artifactoryonline.resource_put)
    def test_set_item_properties(self):
        repo = "libs-releases-local"
        group = "com/scrippsnetworks/engineering-jobdsltest"
        version = "0.0.1.14"
        artifact = "engineering-jobdsltest-service"
        path = tagArtifactory.ArtifactoryPath("https://sni4bees.artifactoryonline.com/sni4bees")
        url = tagArtifactory.get_base_storage_url(repo, group, version, artifact, path)
        properties = {"snapshotless": "true", "git_commit": "3f23aedb2f9e1fa49c783c272ee2ee00502e9904"}
        credentials = "username:password"

        try:
            # Function under test
            tagArtifactory.set_or_delete_item_properties(url, credentials, properties, True)
        except tagArtifactory.VantamException:
            self.fail('Not Expecting VantamException')

    @with_httmock(mocks.artifactoryonline.resource_delete)
    def test_delete_item_properties(self):
        repo = "libs-releases-local"
        group = "com/scrippsnetworks/engineering-jobdsltest"
        version = "0.0.1.14"
        artifact = "engineering-jobdsltest-service"
        path = tagArtifactory.ArtifactoryPath("https://sni4bees.artifactoryonline.com/sni4bees")
        url = tagArtifactory.get_base_storage_url(repo, group, version, artifact, path)
        properties = {"snapshotless": "true", "git_commit": "3f23aedb2f9e1fa49c783c272ee2ee00502e9904"}
        credentials = "username:password"

        try:
            # Function under test
            tagArtifactory.set_or_delete_item_properties(url, credentials, properties, False)
        except tagArtifactory.VantamException:
            self.fail('Not Expecting VantamException')

    @with_httmock(mocks.artifactoryonline.resource_post)
    def test_find_properties_uniqueness(self):
        repo = "libs-releases-local"
        group = "com/scrippsnetworks/engineering-jobdsltest"
        version = "0.0.1.14"
        artifact = "engineering-jobdsltest-service"
        path = tagArtifactory.ArtifactoryPath("https://sni4bees.artifactoryonline.com/sni4bees")
        properties = {"snapshotless": "true", "git_commit": "3f23aedb2f9e1fa49c783c272ee2ee00502e9904"}
        credentials = "username:password"

        # Function under test
        results = tagArtifactory.find_properties_uniqueness(repo, group, artifact, credentials, properties, path)
        expected = 'libs-releases-local/com/scrippsnetworks/engineering-jobdsltest/engineering-jobdsltest-service/0.0.1.68'

        self.assertNotEqual(results, None)
        self.assertIsInstance(results, list)
        self.assertEqual(len(results), 1)
        self.assertEqual(results[0], expected)

    @with_httmock(mocks.artifactoryonline.resource_post)
    @with_httmock(mocks.artifactoryonline.resource_delete)
    @with_httmock(mocks.artifactoryonline.resource_put)
    def test_tag_artifact(self):
        repo = "libs-releases-local"
        group = "com/scrippsnetworks/engineering-jobdsltest"
        version = "0.0.1.14"
        artifact = "engineering-jobdsltest-service"
        path = tagArtifactory.ArtifactoryPath("https://sni4bees.artifactoryonline.com/sni4bees")
        properties = {"snapshotless": "true", "git_commit": "3f23aedb2f9e1fa49c783c272ee2ee00502e9904"}
        credentials = "username:password"

        try:
            # Function under test
            tagArtifactory.tag_artifact(repo, group, version, artifact, credentials, properties, True, path)
        except tagArtifactory.VantamException:
            self.fail('Not Expecting VantamException')