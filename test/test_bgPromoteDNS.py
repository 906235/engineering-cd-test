"""
Tests for the bgPromoteDNS script.
"""

import unittest
import logging
import imp
import os
import bgPromoteDNS
import boto
from moto import mock_route53
from libs.log import setup_logging
setup_logging(logging.ERROR)

# Global Variable
filename = 'bg.properties'

class TestPromoteDNS(unittest.TestCase):

    def tearDown(self):
        if os.path.isfile(filename):
            os.remove(filename)

    def test_get_route53_zone_dev_default_domain(self):
        params = {'TagEnv': 'dev'}

        # Function under test
        result = bgPromoteDNS.route53_zone(params)

        # Verify
        self.assertNotEqual(result, None)
        self.assertIsInstance(result, str)
        self.assertEqual(result, 'awsdev.snops.net.')

    def test_get_route53_zone_dev_defined_domain(self):
        params = {'TagEnv': 'dev', 'Domain' : 'mcd.dev'}

        # Function under test
        result = bgPromoteDNS.route53_zone(params)

        # Verify
        self.assertNotEqual(result, None)
        self.assertIsInstance(result, str)
        self.assertEqual(result, 'mcd.dev.')

    def test_get_route53_cname_string_dev(self):
        params = {'TagEnv': 'dev', 'DNSName' : 'engineeringcdtest'}

        # Function under test
        result = bgPromoteDNS.route53_get_cname_string(params, 'awsdev.snops.net.')

        # Verify
        self.assertNotEqual(result, None)
        self.assertIsInstance(result, str)
        self.assertEqual(result, 'engineeringcdtest-dev.awsdev.snops.net.')

    def test_get_route53_cname_string_qa(self):
        params = {'TagEnv': 'qa', 'DNSName' : 'engineeringcdtest'}

        # Function under test
        result = bgPromoteDNS.route53_get_cname_string(params, 'awsqa.snops.net.')

        # Verify
        self.assertNotEqual(result, None)
        self.assertIsInstance(result, str)
        self.assertEqual(result, 'engineeringcdtest.awsqa.snops.net.')


    @mock_route53
    def test_switch_dns_green_deployement(self):
        params = {'TagEnv': 'dev', 'DNSName' : 'engineeringcdtest', 'DeleteOldStacks': '0'}

        # Pre-conditions (Create CNAME & bg.properties)
        conn53 = boto.connect_route53()
        conncf = boto.connect_cloudformation()
        zone_name = bgPromoteDNS.route53_zone(params)
        cname_string = bgPromoteDNS.route53_get_cname_string(params,zone_name)
        zone = conn53.create_zone(zone_name)
        expected_dns = 'internal-engineeri-ElasticL-V3213JHG6L2U-530608258.us-east-1.elb.amazonaws.com.'
        zone.add_cname(cname_string, 'nothing.scrippsnetworks.com', ttl=60, comment='Added by CD' )
        bg_print(filename, blue = "Null", green = expected_dns)

        # Function under test
        bgPromoteDNS.switch_dns(conn53, conncf, params, zone_name, bgPromoteDNS.read_file(filename))

        # Verify
        self.assertEqual(str(zone.get_cname(cname_string).resource_records[0]), expected_dns)
        properties = bg_read(filename)
        self.assertEqual(properties["blue_dns"], expected_dns)


    @mock_route53
    def test_switch_dns_blue_green_deployment(self):
        params = {'TagEnv': 'dev', 'DNSName' : 'engineeringcdtest', 'DeleteOldStacks': '0'}

        # Pre-conditions (Create CNAME & bg.properties)
        conn53 = boto.connect_route53()
        conncf = boto.connect_cloudformation()
        zone_name = bgPromoteDNS.route53_zone(params)
        cname_string = bgPromoteDNS.route53_get_cname_string(params,zone_name)
        zone = conn53.create_zone(zone_name)
        blue_dns  = 'internal-engineeri-ElasticL-8I3M47AYSH6D-774883919.us-east-1.elb.amazonaws.com'
        green_dns = 'internal-engineeri-ElasticL-MTNJ4B3JYFPZ-307181891.us-east-1.elb.amazonaws.com'
        zone.add_cname(cname_string, 'nothing.scrippsnetworks.com', ttl=60, comment='Added by CD' )
        bg_print(filename, blue = "Null", green = green_dns)
        bg_print(filename, blue = blue_dns, green = "Null")

        # Function under test
        bgPromoteDNS.switch_dns(conn53, conncf, params, zone_name, bgPromoteDNS.read_file(filename))

        # Verify
        self.assertEqual(str(zone.get_cname(cname_string).resource_records[0][:-1]), green_dns)
        properties = bg_read(filename)
        self.assertEqual(properties["blue_dns"],  green_dns)
        self.assertEqual(properties["green_dns"], blue_dns)


def bg_read(filename):
    file = open(filename, 'r')
    s = file.read()
    return dict((field.split('=',1) for field in s.replace("\"", "").replace(" ","").split("\n") if field.strip()))

def bg_print(filename, blue, green):
    """Print blue and/or green ELB DNS to a file. Use for blue/green DNS switchover later."""
    file = open(filename, 'a')
    if blue != "Null":
        file.write("blue_dns = \"%s\"\n" % (blue))
    else:
        file.write("green_dns = \"%s\"\n" % (green))
    file.close()

if __name__ == '__main__':
    unittest.main()
