"""
Tests for the CreateStack script.
"""

import unittest
import logging
import json
import os
import re
import createStack
import boto
from moto import mock_sns, mock_route53, mock_cloudformation, mock_elb, mock_ec2
from libs.log import setup_logging
setup_logging(logging.ERROR)

# Global Variable
filename = os.path.join(os.path.abspath(os.path.curdir), 'bg.properties')
system_param_file = os.path.join(os.path.abspath(os.path.curdir), 'test', 'params-system-dev.json')
app_param_file = os.path.join(os.path.abspath(os.path.curdir), 'test', 'params-dev.json')
host_filename = os.path.join(os.path.abspath(os.path.curdir), 'host.properties')

class TestCreateStack(unittest.TestCase):

    def setUp(self):
        if os.path.isfile(filename):
            os.remove(filename)

        if os.path.isfile(system_param_file):
            os.remove(system_param_file)

        if os.path.isfile(app_param_file):
            os.remove(app_param_file)

        if os.path.isfile(host_filename):
            os.remove(host_filename)


    def test_get_route53_zone_dev_default_domain(self):
        params = {'TagEnv': 'dev'}

        # Function under test
        result = createStack.route53_zone(params)

        # Verify
        self.assertNotEqual(result, None)
        self.assertIsInstance(result, str)
        self.assertEqual(result, 'awsdev.snops.net.')

    def test_get_route53_zone_dev_defined_domain(self):
        params = {'TagEnv': 'dev', 'Domain' : 'mcd.dev'}

        # Function under test
        result = createStack.route53_zone(params)

        # Verify
        self.assertNotEqual(result, None)
        self.assertIsInstance(result, str)
        self.assertEqual(result, 'mcd.dev.')

    def test_get_route53_cname_string_dev(self):
        params = {'TagEnv': 'dev', 'DNSName' : 'engineeringcdtest'}

        # Function under test
        result = createStack.route53_get_cname_string(params, 'awsdev.snops.net.')

        # Verify
        self.assertNotEqual(result, None)
        self.assertIsInstance(result, str)
        self.assertEqual(result, 'engineeringcdtest-dev.awsdev.snops.net.')

    def test_get_route53_cname_string_qa(self):
        params = {'TagEnv': 'qa', 'DNSName' : 'engineeringcdtest'}

        # Function under test
        result = createStack.route53_get_cname_string(params, 'awsqa.snops.net.')

        # Verify
        self.assertNotEqual(result, None)
        self.assertIsInstance(result, str)
        self.assertEqual(result, 'engineeringcdtest.awsqa.snops.net.')

    @mock_route53
    def test_route53_check_cname_added(self):
        params = {'TagEnv': 'qa', 'DNSName' : 'engineeringcdtest'}
        conn = boto.connect_route53()

        # Pre-conditions
        zone_name = createStack.route53_zone(params)
        expected_cname = createStack.route53_get_cname_string(params,zone_name)
        conn.create_zone(zone_name)

        # Function under test
        createStack.route53_check(conn, params)

        # Verify CNAME created
        zone = conn.get_zone(zone_name)
        self.assertNotEqual(zone, None)
        cname = zone.get_cname(expected_cname)
        self.assertNotEqual(cname, None)
        self.assertEqual(cname.name,expected_cname)


    @mock_sns
    def test_subscribe_to_topic_when_topic_not_existing(self):
        params = {'StackName':  'engineeringcdtest-dev',
                  'TagOwner': 'egagne@scrippsnetworks.com',
                  'BusinessUnitEmail' : 'dl-contentdeliveryengineering@scrippsnetworks.com',
                  'AlternateBusinessUnitEmail' : ''}
        conn = boto.connect_sns()

        # Function under test
        createStack.subscribe_to_topic(conn,params)
        expectedTopicArn = 'arn:aws:sns:us-east-1:123456789012:engineeringcdtest-dev'

        # Verify Topic is created
        response = conn.get_all_topics()
        self.assertIsInstance(response, dict)
        self.assertEqual(len(response['ListTopicsResponse']['ListTopicsResult']['Topics']), 1)
        self.assertEqual(response['ListTopicsResponse']['ListTopicsResult']['Topics'][0]['TopicArn'],expectedTopicArn)
        self.assertEqual(params['Snstopic'], expectedTopicArn)

        # Verify all Emails are subscribed
        response = conn.get_all_subscriptions_by_topic(expectedTopicArn)
        self.assertIsInstance(response, dict)
        self.assertEqual(len(response['ListSubscriptionsByTopicResponse']['ListSubscriptionsByTopicResult']['Subscriptions']), 2)
        subscriptions = response['ListSubscriptionsByTopicResponse']['ListSubscriptionsByTopicResult']['Subscriptions']
        endpoints = [x['Endpoint'] for x in subscriptions]
        self.assertTrue(params['TagOwner'] in endpoints)
        self.assertTrue(params['BusinessUnitEmail'] in endpoints)
        self.assertFalse(params['AlternateBusinessUnitEmail'] in endpoints)

    @mock_sns
    def test_subscribe_to_topic_when_topic_exist(self):
        params = {'StackName':  'engineeringcdtest-dev',
                  'TagOwner': 'egagne@scrippsnetworks.com',
                  'BusinessUnitEmail' : 'dl-contentdeliveryengineering@scrippsnetworks.com',
                  'AlternateBusinessUnitEmail' : ''}
        conn = boto.connect_sns()

        # Pre-conditions
        conn.create_topic(params['StackName'])

        # Function under test
        createStack.subscribe_to_topic(conn,params)
        expectedTopicArn = 'arn:aws:sns:us-east-1:123456789012:engineeringcdtest-dev'

        # Verify there is just 1 Topic
        response = conn.get_all_topics()
        self.assertIsInstance(response, dict)
        self.assertEqual(len(response['ListTopicsResponse']['ListTopicsResult']['Topics']), 1)

        # Verify the 2 subscriptions are added to existing Topic
        response = conn.get_all_subscriptions_by_topic(expectedTopicArn)
        self.assertIsInstance(response, dict)
        self.assertEqual(len(response['ListSubscriptionsByTopicResponse']['ListSubscriptionsByTopicResult']['Subscriptions']), 2)

    @mock_sns
    def test_subscribe_to_topic_when_topic_and_subscribers_exist(self):
        params = {'StackName':  'engineeringcdtest-dev',
                  'TagOwner': 'egagne@scrippsnetworks.com',
                  'BusinessUnitEmail' : 'dl-contentdeliveryengineering@scrippsnetworks.com',
                  'AlternateBusinessUnitEmail' : ''}
        conn = boto.connect_sns()

        # Pre-conditions
        response = conn.create_topic(params["StackName"])
        topicArn = response['CreateTopicResponse']['CreateTopicResult']['TopicArn']
        conn.subscribe(topicArn,'email',params['TagOwner'])
        conn.subscribe(topicArn,'email',params['BusinessUnitEmail'])

        # Function under test
        createStack.subscribe_to_topic(conn,params)
        self.assertEqual(params['Snstopic'], topicArn)

        # Verify there are only 2 subscriptions to the Topic
        response = conn.get_all_subscriptions_by_topic(topicArn)
        self.assertIsInstance(response, dict)
        self.assertEqual(len(response['ListSubscriptionsByTopicResponse']['ListSubscriptionsByTopicResult']['Subscriptions']), 2)

    @mock_cloudformation
    @mock_route53
    @mock_elb
    def test_stack_check_no_existing_stack(self):
        json_data = open('./test/params-legacy-dev.json')
        params = json.load(json_data)

        # Pre-conditions
        conn = boto.connect_cloudformation()
        conn53 = boto.connect_route53()
        conn_elb = boto.connect_elb()
        createStack.check_autoscaling(params)
        params['Snstopic'] = 'arn:aws:sns:us-east-1:123456789012:' + params['StackName']
        expected_stack_name = params['StackName'] + '-1'

        # Function under test
        createStack.stack_check(conn,conn53, conn_elb, params, 'file://' + os.getcwd() + '/cdCloudForm.template')

        # Verify
        self.assertEqual(params['StackName'], expected_stack_name)
        self.assertNotEqual(conn.describe_stacks(expected_stack_name), None)

    @mock_cloudformation
    @mock_route53
    @mock_elb
    def test_stack_check_stack1_exist(self):
        json_data = open('./test/params-legacy-dev.json')
        params = json.load(json_data)

        # Pre-conditions (Create Stack-1 & Application CNAME entry for Stack-1)
        conn = boto.connect_cloudformation()
        conn53 = boto.connect_route53()
        conn_elb = boto.connect_elb()
        createStack.check_autoscaling(params)
        params['Snstopic'] = 'arn:aws:sns:us-east-1:123456789012:' + params['StackName']
        template = 'file://' + os.getcwd() + '/cdCloudForm.template'
        stack_name = params['StackName']
        params['StackName'] = stack_name + '-1'
        createStack.create_stack(conn,params,template) # Create Stack-1
        params['StackName'] = stack_name
        expected_stack_name = stack_name + '-2'
        zone_name = createStack.route53_zone(params)
        expected_cname = createStack.route53_get_cname_string(params,zone_name)
        expected_blue_dns = 'internal-engineeri-ElasticL-V3213JHG6L2U-530608258.us-east-1.elb.amazonaws.com.'
        zone = conn53.create_zone(zone_name)
        zone.add_cname(expected_cname, expected_blue_dns, ttl=60, comment='Added by CD' )

        # Function under test
        createStack.stack_check(conn, conn53, conn_elb, params, template)

        # Verify
        self.assertEqual(params['StackName'], expected_stack_name)
        self.assertNotEqual(conn.describe_stacks(expected_stack_name), None)
        properties = bg_read(filename)
        self.assertEqual(properties["blue_dns"], expected_blue_dns)

    @mock_ec2
    def test_compile_params_app_params_overwrite_system_params(self):

        # Pre-conditions
        conn = boto.connect_vpc('the_key', 'the_secret')
        vpc = conn.create_vpc("10.0.0.0/16")
        subnet1 = conn.create_subnet(vpc.id, "10.0.0.0/18", availability_zone='us-west-1c')
        subnet2 = conn.create_subnet(vpc.id, "10.0.10.0/18", availability_zone='us-west-1b')

        args = createStack.arg_parse_module(['--params', './test/params-legacy-dev.json', '--version', '1.1.35'])
        json_data = open(args.params)
        data = json.load(json_data)

        data['Subnet1'] = subnet1.id
        data['Subnet2'] = subnet2.id

        # Function under test
        result = createStack.compile_params(conn, data, 'params', args)

        # Verify
        self.assertNotEqual(result, None)
        self.assertIsInstance(result, dict)
        self.assertEqual(result['AppName'], 'engineering-cdtest-service')
        self.assertEqual(result['IAMRole'], 'ct-avail-ec2-role')
        self.assertEqual(result['Subnet1'], subnet1.id)
        self.assertEqual(result['Subnet2'], subnet2.id)
        self.assertEqual(result['Securitygrp'], 'sg-ca914ab5')
        self.assertIn(result['AvailZone1'], ['us-west-1c', 'us-west-1b'])
        self.assertIn(result['AvailZone2'], ['us-west-1c', 'us-west-1b'])
        self.assertEqual(result['sumo_deploy::dev_team'], result['TagOwner'])
        self.assertEqual(result['springboot_deploy::app_version'], '1.1.35')
        self.assertEqual(result['springboot_deploy::service_port'], '8080')
        self.assertEqual(result['springboot_deploy::health_uri'], '/info')
        self.assertEqual(result['BusinessUnitEmail'], 'dl-contentdeliveryengineering@scrippsnetworks.com')

    @mock_ec2
    def test_compile_params_system_default_account(self):

        # Pre-conditions
        conn = boto.connect_vpc('the_key', 'the_secret')
        vpc = conn.create_vpc("10.0.0.0/16")
        subnet1 = conn.create_subnet(vpc.id, "10.0.0.0/18", availability_zone='us-west-1c')
        subnet2 = conn.create_subnet(vpc.id, "10.0.10.0/18", availability_zone='us-west-1b')

        my_file = open('./params/params-system-dev.json')
        my_file_data = json.load(my_file)
        my_file_data['Account']['default']['Subnet1'] = subnet1.id # overwrite the subnets for mocking
        my_file_data['Account']['default']['Subnet2'] = subnet2.id
        my_new_file = open(system_param_file, 'a')
        my_new_file.write(json.dumps(my_file_data))
        my_new_file.close()

        params_apps = {
            "AppName": "engineering-cdtest-service",
            "DNSName": "engineeringcdtest",
            "Healthcheck": "http:8080/info",
            "InstanceSize": "t2.small",
            "MaxSize": "3",
            "Platform": "springboot",
            "StackName": "engineeringcdtest-dev",
            "TagBusUnit": "contentdelivery",
            "TagEnv": "dev",
            "TagOwner": "EGagne-c@scrippsnetworks.com",
            "TagProject": "engineeringcdtest",
            "springboot_deploy::app_deploy_file_ext": "jar",
            "springboot_deploy::app_deploy_name": "engineering-cdtest-service",
            "springboot_deploy::extra_startup_params": "-Dspring.profiles.active=dev",
            "springboot_deploy::prefix_url": "https://sni4bees.artifactoryonline.com/sni4bees/public/com/scrippsnetworks/engineering-cdtest"
        }

        my_app_file = open(app_param_file, 'a')
        my_app_file.write(json.dumps(params_apps))
        my_app_file.close()

        args = createStack.arg_parse_module(['--params', './test/params-dev.json', '--version', '1.1.35'])
        json_data = open(args.params)
        data = json.load(json_data)

        # Function under test
        result = createStack.compile_params(conn, data, 'test', args)

        # Verify
        self.assertNotEqual(result, None)
        self.assertIsInstance(result, dict)
        self.assertEqual(result['AppName'], 'engineering-cdtest-service')
        self.assertEqual(result['IAMRole'], 'ct-avail-ec2-role')
        self.assertEqual(result['Subnet1'], subnet1.id)
        self.assertEqual(result['Subnet2'], subnet2.id)
        self.assertEqual(result['Elbsubnet1'], 'subnet-90bc8ffd')
        self.assertEqual(result['Elbsubnet2'], 'subnet-45a39028')
        self.assertEqual(result['Securitygrp'], 'sg-ca914aa5')
        self.assertEqual(result['KeyName'], 'hybrid-d')
        self.assertIn(result['AvailZone1'], ['us-west-1c', 'us-west-1b'])
        self.assertIn(result['AvailZone2'], ['us-west-1c', 'us-west-1b'])
        self.assertEqual(result['sumo_deploy::dev_team'], result['TagOwner'])
        self.assertEqual(result['springboot_deploy::app_version'], '1.1.35')
        self.assertEqual(result['springboot_deploy::service_port'], '8080')
        self.assertEqual(result['springboot_deploy::health_uri'], '/info')
        self.assertEqual(result['BusinessUnitEmail'], 'dl-contentdeliveryengineering@scrippsnetworks.com')
        self.assertNotIn('loggly', result['classes'])


    @mock_ec2
    def test_compile_params_system_account_447434275168(self):
        # Pre-conditions
        conn = boto.connect_vpc('the_key', 'the_secret')
        vpc = conn.create_vpc("10.0.0.0/16")
        subnet1 = conn.create_subnet(vpc.id, "10.0.0.0/18", availability_zone='us-west-1c')
        subnet2 = conn.create_subnet(vpc.id, "10.0.10.0/18", availability_zone='us-west-1b')

        my_file = open('./params/params-system-dev.json')
        my_file_data = json.load(my_file)
        my_file_data['Account']['447434275168']['Subnet1'] = subnet1.id  # overwrite the subnets for mocking
        my_file_data['Account']['447434275168']['Subnet2'] = subnet2.id
        my_new_file = open(system_param_file, 'a')
        my_new_file.write(json.dumps(my_file_data))
        my_new_file.close()

        params_apps = {
            "AppName": "engineering-cdtest-service",
            "DNSName": "engineeringcdtest",
            "Healthcheck": "http:8080/info",
            "InstanceSize": "t2.small",
            "MaxSize": "3",
            "Platform": "springboot",
            "StackName": "engineeringcdtest-dev",
            "TagBusUnit": "contentdelivery",
            "TagEnv": "dev",
            "TagOwner": "EGagne-c@scrippsnetworks.com",
            "TagProject": "engineeringcdtest",
            "springboot_deploy::app_deploy_file_ext": "jar",
            "springboot_deploy::app_deploy_name": "engineering-cdtest-service",
            "springboot_deploy::extra_startup_params": "-Dspring.profiles.active=dev",
            "springboot_deploy::prefix_url": "https://sni4bees.artifactoryonline.com/sni4bees/public/com/scrippsnetworks/engineering-cdtest",
            "AccountRoleArn": "arn:aws:iam::447434275168:role/mam-jenkins"
        }

        my_app_file = open(app_param_file, 'a')
        my_app_file.write(json.dumps(params_apps))
        my_app_file.close()

        args = createStack.arg_parse_module(['--params', './test/params-dev.json', '--version', '1.1.35'])
        json_data = open(args.params)
        data = json.load(json_data)

        # Function under test
        result = createStack.compile_params(conn, data, 'test', args)

        # Verify
        self.assertNotEqual(result, None)
        self.assertIsInstance(result, dict)
        self.assertEqual(result['AppName'], 'engineering-cdtest-service')
        self.assertEqual(result['IAMRole'], 'ct-avail-ec2-role')
        self.assertEqual(result['Subnet1'], subnet1.id)
        self.assertEqual(result['Subnet2'], subnet2.id)
        self.assertEqual(result['Elbsubnet1'], 'subnet-bdef03e5')
        self.assertEqual(result['Elbsubnet2'], 'subnet-02c3dd29')
        self.assertEqual(result['Securitygrp'], 'sg-8bd6b4f3')
        self.assertEqual(result['KeyName'], 'kpd16068')
        self.assertIn(result['AvailZone1'], ['us-west-1c', 'us-west-1b'])
        self.assertIn(result['AvailZone2'], ['us-west-1c', 'us-west-1b'])
        self.assertEqual(result['sumo_deploy::dev_team'], result['TagOwner'])
        self.assertEqual(result['springboot_deploy::app_version'], '1.1.35')
        self.assertEqual(result['springboot_deploy::service_port'], '8080')
        self.assertEqual(result['springboot_deploy::health_uri'], '/info')
        self.assertEqual(result['BusinessUnitEmail'], 'dl-contentdeliveryengineering@scrippsnetworks.com')
        self.assertNotIn('loggly', result['classes'])


    @mock_ec2
    def test_compile_params_system_invalid_account(self):

        # Pre-conditions
        conn = boto.connect_vpc('the_key', 'the_secret')
        params_apps = {
            "AppName": "engineering-cdtest-service",
            "DNSName": "engineeringcdtest",
            "Healthcheck": "http:8080/info",
            "InstanceSize": "t2.small",
            "MaxSize": "3",
            "Platform": "springboot",
            "StackName": "engineeringcdtest-dev",
            "TagBusUnit": "contentdelivery",
            "TagEnv": "dev",
            "TagOwner": "EGagne-c@scrippsnetworks.com",
            "TagProject": "engineeringcdtest",
            "springboot_deploy::app_deploy_file_ext": "jar",
            "springboot_deploy::app_deploy_name": "engineering-cdtest-service",
            "springboot_deploy::extra_startup_params": "-Dspring.profiles.active=dev",
            "springboot_deploy::prefix_url": "https://sni4bees.artifactoryonline.com/sni4bees/public/com/scrippsnetworks/engineering-cdtest",
            "AccountRoleArn": "arn:aws:iam::invalid:role/mam-jenkins"
        }

        my_app_file = open(app_param_file, 'a')
        my_app_file.write(json.dumps(params_apps))
        my_app_file.close()

        args = createStack.arg_parse_module(['--params', './test/params-dev.json', '--version', '1.1.35'])
        json_data = open(args.params)
        data = json.load(json_data)

        try:
            # Function under test
            createStack.compile_params(conn, data, 'params', args)
        except createStack.VantamException:
            pass
        else:
            self.fail('Expecting VantamException')

    @mock_ec2
    def test_construct_create_stack_params(self):

        # Pre-conditions
        conn = boto.connect_vpc('the_key', 'the_secret')
        vpc = conn.create_vpc("10.0.0.0/16")
        subnet1 = conn.create_subnet(vpc.id, "10.0.0.0/18", availability_zone='us-west-1c')
        subnet2 = conn.create_subnet(vpc.id, "10.0.10.0/18", availability_zone='us-west-1b')

        args = createStack.arg_parse_module(['--params', './test/params-legacy-dev.json', '--version', '1.1.35'])
        json_data = open(args.params)
        data = json.load(json_data)
        createStack.check_autoscaling(data)

        # Function under test
        result = createStack.construct_create_stack_params(data)

        # Verify
        self.assertNotEqual(result, None)
        self.assertIsInstance(result, list)
        singleDict = dict(result)
        self.assertIn('AppName', singleDict)
        self.assertIn('IAMRole', singleDict)
        self.assertIn('Subnet1', singleDict)
        self.assertIn('Subnet2', singleDict)
        self.assertIn('Securitygrp', singleDict)
        self.assertIn('AvailZone1', singleDict)
        self.assertIn('AvailZone2', singleDict)
        self.assertIn('Minsize', singleDict)
        self.assertIn('TagEnv', singleDict)
        self.assertIn('TagOwner', singleDict)
        self.assertIn('TagBusUnit', singleDict)
        self.assertIn('TagProject', singleDict)
        self.assertIn('IAMRole', singleDict)
        self.assertIn('AMImage', singleDict)
        self.assertIn('InstanceSize', singleDict)
        self.assertIn('KeyName', singleDict)
        self.assertIn('Elbsubnet1', singleDict)
        self.assertIn('Elbsubnet2', singleDict)
        self.assertIn('ElbPort', singleDict)
        self.assertIn('ElbFrontPort', singleDict)
        self.assertIn('ElbProto', singleDict)
        self.assertIn('ConnTimeout', singleDict)
        self.assertIn('CrossZone', singleDict)
        self.assertIn('InstProto', singleDict)
        self.assertIn('Sticky', singleDict)
        self.assertIn('CertARN', singleDict)
        self.assertIn('Healthcheck', singleDict)


    @mock_elb
    @mock_ec2
    def test_stack_results_print_with_no_ec2_and_disable_ip_transform_flag(self):

        # Pre-conditions
        ec2_inst = "none"
        elb = boto.connect_elb()

        args = createStack.arg_parse_module(['--params', './test/deploy/params-dev.json', 
                                             '--version', '1.1.35', 
                                             '--disable-ip-transform'])

        # Function under test
        createStack.stack_results_print(ec2_inst, elb, 1, args.disable_ip_transform)

        # Get values from the file
        ten_dot = re.compile(r'10\..*')
        file_values = read_host_properties()

        # Verify
        for key in ['ACCEPTANCE_HOST1', 'ACCEPTANCE_HOST2', 'EBL_URL']:
            if file_values.has_key(key):
                self.assertRegexpMatches(file_values[key], ten_dot)


    @mock_elb
    @mock_ec2
    def test_stack_results_print_with_no_ec2_and_not_disable_ip_transform_flag(self):

        # Pre-conditions
        ec2_inst = "none"
        elb = boto.connect_elb()

        args = createStack.arg_parse_module(['--params', './test/deploy/params-dev.json', 
                                             '--version', '1.1.35'])

        # Function under test
        createStack.stack_results_print(ec2_inst, elb, 1, args.disable_ip_transform)

        # Get values from the file
        ten_dot = re.compile(r'10\..*')
        file_values = read_host_properties()

        # Verify
        for key in ['ACCEPTANCE_HOST1', 'ACCEPTANCE_HOST2', 'EBL_URL']:
            if file_values.has_key(key):
                self.assertRegexpMatches(file_values[key], ten_dot)


    @mock_elb
    @mock_ec2
    def test_stack_results_print_with_ec2_and_disable_ip_transform_flag(self):

        # Pre-conditions
        ec2_inst = "10.50.8.10"
        elb = boto.connect_elb()

        args = createStack.arg_parse_module(['--params', './test/deploy/params-dev.json', 
                                             '--version', '1.1.35'])

        # Function under test
        createStack.stack_results_print(ec2_inst, elb, 1, args.disable_ip_transform)

        # Get values from the file
        ten_dot = re.compile(r'10\..*')
        file_values = read_host_properties()

        # Verify
        for key in ['ACCEPTANCE_HOST1', 'ACCEPTANCE_HOST2', 'EBL_URL']:
            if file_values.has_key(key):
                self.assertNotRegexpMatches(file_values[key], ten_dot)


    @mock_elb
    @mock_ec2
    def test_stack_results_print_with_ec2_and_not_disable_ip_transform_flag(self):

        # Pre-conditions
        ec2_inst = "10.50.8.10"
        elb = boto.connect_elb()

        args = createStack.arg_parse_module(['--params', './test/deploy/params-dev.json', 
                                             '--version', '1.1.35'])

        # Function under test
        createStack.stack_results_print(ec2_inst, elb, 1, args.disable_ip_transform)

        # Get values from the file
        ten_dot = re.compile(r'10\..*')
        file_values = read_host_properties()

        # Verify
        for key in ['ACCEPTANCE_HOST1', 'ACCEPTANCE_HOST2', 'EBL_URL']:
            if file_values.has_key(key):
                self.assertNotRegexpMatches(file_values[key], ten_dot)


def read_host_properties(filename=host_filename):
    """
    read the host.properties file and put values into dict
    """
    f = open(filename, 'r')
    file_values = dict()
    for line in f.readlines():
        key, value = line.split('=')
        file_values[key.strip()] = value.strip()
    f.close()
    return file_values


def bg_read(filename):
    file = open(filename, 'r')
    s = file.read()
    return dict((field.split('=',1) for field in s.replace("\"", "").replace(" ","").split("\n") if field.strip()))


if __name__ == '__main__':
    unittest.main()

