engineering-CD (VanTAM) Tools
==================================
Engineering-CD are our Continous Delevery (CD) tools for auto deploying in a pipeline to AWS.
Reading this will quickly help you use the tools.


## Environment Variables
The following environment variables need to be present if running locally. These contain values for your AWS API keys.

DEV_AWS_ACCESS_KEY_ID, 
DEV_AWS_SECRET_ACCESS_KEY, 
QA_AWS_ACCESS_KEY_ID, 
QA_AWS_SECRET_ACCESS_KEY, 
PROD_AWS_ACCESS_KEY_ID, 
PROD_AWS_SECRET_ACCESS_KEY,  

## Deploy Using the Tools
Command to deploy application stack:
```sh
./pylibs ./createStack.py --params <../deploy/params-env.json> --version <version_number> [--template FILE | URL]
```

--params
This is the path to your parameters or input file. This files tells the CD tools everything it needs to do and deploy. See wiki via the link below for the details on this file:
https://scrippsnetworks.atlassian.net/wiki/display/ENG/CD+Parameters+File

--version
You must pass in the version of the application you want to deploy. This is used to find the correct version of the deployment artifact to deploy within artifactory.

--template
Optional argument to allow passing in a template other than the default (s3://sni-puppet/cdCloudForm.template, which is used if the argument is omitted).
FILE is a local file to the filesystem or Jenkins workspace passed along with the code. Format: ./my.template, /tmp/my-alternate.template
URL is the location in an S3 bucket using the following formats: s3://bucket-name/cf.template, http://s3.amazonaws.com/bucket-name/cf.template.

## Blue/Green DNS Switch Over
Command to promote/switch the DNS:
```sh
./pylibs ./bgPromoteDNS.py --params <../deploy/params-env.json>
```
After deployment a 'bg.properties' file is created. This will contain what stack/ELB of this application is blue/live or green/standby. The most recent stack will always be in the green/standby state until the promotion of the DNS occurs.

There can only be two stacks, blue/live and green/standby. If a new stack is spun up and there are already two stacks. The current green/standby stack is deleted and replaced by the new one spun up. 

Once the DNS promotion command is run the 'bg.properties' file will be updated. So if something did go wrong with the new version of the application you can run the DNS promotion command again to switch back.

## Running Unit Tests & Pylint (Using Docker container as a runtime environment)
- Build the engineering-cd Docker image:
```
docker build -t engineering-cd .
```
- Launch the engineering-cd runtime interactive container by mounting the engineering-cd directory as a volume (absolute path):
```
docker run -it -v /Users/egagne/git_root/engineering-cd:/src engineering-cd:latest /bin/bash
```
- Now into the Docker terminal, run the unittests & pylint:
```
root@ea886cea5989:/# cd src
root@ea886cea5989:/src# python -m unittest discover -s ./test -p "test_*.py" -v
root@ea886cea5989:/src# pylint --rcfile=.pylint.cfg $(find . -maxdepth 2 -name "*.py" -not -path "./test/*" -not -path "./scripts/*" -not -path "./libs/*" -print) > pylint.log
```


## Running Unit Tests (Installing necessary tools on your local machine)
- Install Python 2.7:
```
pip install python27
```
- Install Boto 2.39:
```
pip install boto-2.39
```
- Install Moto (Boto Mocking module) https://github.com/spulec/moto:
```
pip install moto
```
- Install httmock:
```
pip install httmock
```
- Run tests (from engineering-cd dir):
```
export PYTHONPATH=/Library/Python/2.7/site-packages/boto
python -m unittest discover -s ./test -p "test_*.py" -v
```
   
## Analyzing Code
- Install astroid
```
pip install astroid
```
- Install pylint
```
pip install pylint
```
- Run pylint on code
```
pylint --rcfile=.pylint.cfg $(find . -maxdepth 2 -name "*.py" -not -path "./test/*" -not -path "./scripts/*" -not -path "./libs/*" -print) > pylint.log
```
   
## Generating Documentation
- Run doc generator
```
pydoc -w $(find . -maxdepth 2 -name "*.py" -not -path "./test/*" -not -path "./scripts/*" -not -path "./libs/*" -print)
```



