#!/usr/bin/env python

"""This module is used to validate autoscaling parameters and set default values if necessary."""


def init_cpu_auto_scaling(data):
    """ Function to initialize Cpu AutoScaling parameters to none if not defined.
        :param data: The configuration parameters
        :type data: Dictionary
    """
    for k in ['UpMinSize', 'UpMaxSize', 'UpRecurrence', 'DownMinSize', 'DownMaxSize', 'DownRecurrence']:
        if not data['AutoScaling'].has_key(k):
            data['AutoScaling'][k] = "none"


def init_scheduled_auto_scaling(data):
    """ Function to initialize Scheduled AutoScaling parameters to none if not defined.
        :param data: The configuration parameters
        :type data: Dictionary
    """
    for k in ['CpuUp', 'CpuDown', 'CpuMonitorUpSecs', 'CpuMonitorDownSecs', \
              'CpuMonitorUpPeriods', 'CpuMonitorDownPeriods']:
        if not data['AutoScaling'].has_key(k):
            data['AutoScaling'][k] = "none"


def check_autoscaling(data):
    """ Function to check/initialize Auto Scaling Parameters.
        :param data: The configuration parameters
        :type data: Dictionary
    """
    if "AutoScaling" not in data:
        data["AutoScaling"] = dict()
        data["AutoScaling"]["Type"] = "none"
    elif "Type" not in data["AutoScaling"]:
        data["AutoScaling"]["Type"] = "none"

    if data["AutoScaling"]["Type"] == "none":
        init_cpu_auto_scaling(data)
        init_scheduled_auto_scaling(data)
    elif data["AutoScaling"]["Type"] == "cpu":
        init_cpu_auto_scaling(data)
    elif data["AutoScaling"]["Type"] == "scheduled":
        init_scheduled_auto_scaling(data)
