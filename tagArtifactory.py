#!/usr/bin/env python

""" This module is used to tag artifacts into Artifactory """

import json
import re
import argparse
import logging
import os
import requests

from requests.auth import HTTPBasicAuth
from libs.log import module_logger, setup_logging

# Logging
LOG = module_logger(__name__)
setup_logging(logging.INFO)


# exception class
class VantamException(Exception):
    """ Exception class """
    pass


class ArtifactoryPath(object):
    """ Class that encapsulate the different Artifactory path"""
    def __init__(self, base_path):
        self.base_path = base_path
        self.base_path_storage = base_path + "/api/storage"
        self.base_path_aql = base_path + "/api/search/aql"

    def get_base_path_storage(self):
        """ Getter for base path storage """
        return self.base_path_storage

    def get_base_path_aql(self):
        """ Getter for base path aql """
        return self.base_path_aql


def url_type(param):
    """Function to validate ulr parameter
       :param param: The parameter to validate
       :returns the parameter or an exception if not valid
    """
    url_regex = re.compile(r'^https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,}')
    if not url_regex.match(param):
        msg = "Artifactory Url must be properly formatted."
        raise argparse.ArgumentTypeError(msg)
    return param


def credential_type(param):
    """Function to validate credential parameter
       :param param: The parameter to validate
       :returns the parameter or an exception if not valid
    """
    if ":" not in param:
        msg = "Credentials must has the format 'username:password'"
        raise argparse.ArgumentTypeError(msg)
    return param


def version_type(param):
    """Function to validate version parameter
       :param param: The parameter to validate
       :returns the parameter or an exception if not valid
    """
    version_regex = re.compile(r'\d{1,3}.\d{1,3}.\d{1,3}(?:-SNAPSHOT|.\d{1,4})?$')
    if not version_regex.match(param):
        msg = """Version number must has the format 'major.minor.patch' each from 1 to 3 digits,
              or 'major.minor.patch-SNAPSHOT' or 'major.minor.patch.build' where build can be from 1 to 4 digits' """
        raise argparse.ArgumentTypeError(msg)
    return param


def json_file_type(param):
    """Function to validate if the filename contain .json as extension
       :param param: The parameter to validate
       :returns the parameter or an exception if not valid
    """
    file_regex = re.compile(r'.*.json$')
    if not file_regex.match(param):
        msg = "Tag file must finish with .json extension"
        raise argparse.ArgumentTypeError(msg)
    return param


def kv_properties_type(param):
    """Function to validate the key/value string parameter
       :param param: The parameter to validate
       :returns the parameter or an exception if not valid
    """
    regex = re.compile(r'^\[([^,\]]+).([^\]]+)')
    if not regex.match(param):
        msg = 'Expecting an array of "Key" : "Value" separated by a comma'
        raise argparse.ArgumentTypeError(msg)
    return param


def str2bool(param):
    """Function to convert a string to boolean
       :param param: The parameter to validate
       :returns boolean True if string equal "true"
    """
    return param.lower() in "true"


def module_arg_parse():
    """Function to parse the module arguments"""
    LOG.info('Getting Arguments')
    parser = argparse.ArgumentParser(description='Content-Delivery Tagging Artifacts tool')
    parser.register('type', 'bool', str2bool)
    parser.add_argument('--url',
                        default=os.environ['ARTIFACTORY_API'],
                        type=url_type,
                        help='Artifactory base url')
    parser.add_argument('--params',
                        required=True,
                        type=json_file_type,
                        help='JSON Tags file')
    parser.add_argument('--version',
                        required=True,
                        type=version_type,
                        help='Application Version in the form of x.x.x or x.x.x-SNAPSHOT or x.x.x.x')
    parser.add_argument('--credentials',
                        type=credential_type,
                        default=os.environ['ARTIFACTORY_USERNAME'] + ':' + os.environ['ARTIFACTORY_PASSWORD'],
                        help='Artifactory credentials in the form of username:password')
    parser.add_argument('--kvProperties',
                        required=True,
                        type=kv_properties_type,
                        help="Array of Key:Value Properties")
    parser.add_argument('--unique',
                        default=False,
                        type='bool',
                        help="Properties will be treated as unique for all the versions of the artifact")

    args = parser.parse_args()
    return args


def validate_json_file_content(json_contents):
    """Function to validate JSON Tag File Content.
       :param json_contents: The json file content to validate
       :returns None or throw exception if not valid
    """
    repo_regex = re.compile(r'^(npm|libs)-releases-local$')
    repo_flag = True if "repo" in json_contents and repo_regex.match(json_contents["repo"]) else False

    group_regex = re.compile(r'[a-zA-Z/-]+')
    group_flag = True if "group" in json_contents and group_regex.match(json_contents["group"]) else False

    artifacts_flag = True if "artifacts" in json_contents and len(json_contents["artifacts"]) > 0 else False

    if not repo_flag or not group_flag or not artifacts_flag:
        raise VantamException("missing mandatory key in json file")


def stringify_properties_set(kv_properties):
    """Function to stringify the key/value properties.
       :param kv_properties: The key/value properties
       :type kv_properties: Dictionary
       :returns String of key=value concatenated by '|'
    """
    query_pairs = ["%s=%s" % (k, v) for k, v in kv_properties.items()]
    return '|'.join(query_pairs)


def stringify_properties_delete(kv_properties):
    """Function to stringify the key/value properties.
       :param kv_properties: The key/value properties
       :type kv_properties: Dictionary
       :returns String of key=value concatenated by ','
    """
    query_pairs = [k for k in kv_properties.keys()]
    return ','.join(query_pairs)


def stringify_properties_aql(kv_properties):
    """Function to stringify the key/value properties.
       :param kv_properties: The key/value properties
       :type kv_properties: Dictionary
       :returns String of @key:value concatenated by ','
    """
    query_pairs = ['{"@%s" : "%s"}' % (k, v) for k, v in kv_properties.items()]
    return ','.join(query_pairs)


def get_base_storage_url(repo, group, version, artifact, path):
    """Function to construct the 'GET' Artifactory storage URL.
       :param repo: The Artifactory repository
       :param group: The Artifactory group
       :param version: The artifact version
       :param artifact: The artifact name
       :type repo: String
       :type group: String
       :type version: String
       :type artifact: String
       :returns The Artifactory URL
    """
    base_url = path.get_base_path_storage() + '/' + repo + '/' + group + '/' + artifact

    if "libs-" in repo:
        # SpringBoot Tagging Pattern
        base_url = base_url + '/' + version
    else:
        # Npm Tagging Pattern
        base_url = base_url + '-' + version + '.tgz'

    return base_url


def to_base_storage_url(uri, path):
    """Function to construct the 'PUT' Artifactory storage URL.
       :param uri: The uri to concatenate
       :type uri: String
       :returns The Artifactory URL
    """
    return path.get_base_path_storage() + '/' + uri


def tag_artifact(repo, group, version, artifact, credentials, kv_properties, unique, path):
    """Function to tag the Artifact into Artifactory.
       :param repo: The Artifactory repository
       :param group: The Artifactory group
       :param version: The artifact version
       :param artifact: The artifact name
       :param credentials: The username:password to access Artifactory account
       :param kv_properties: The properties to set on the Artifact
       :param unique: Flag indicating if the properties must be unique within every versions of the Artifact.
       :type repo: String
       :type group: String
       :type version: String
       :type artifact: String
       :type credential: String
       :type kv_properties: Dictionary
       :type unique: Boolean
    """
    if unique:
        uris = find_properties_uniqueness(repo, group, artifact, credentials, kv_properties, path)

        if len(uris) > 0:
            for uri in uris:
                set_or_delete_item_properties(to_base_storage_url(uri, path), credentials, kv_properties, False)

    set_or_delete_item_properties(get_base_storage_url(repo, group, version, artifact, path),
                                  credentials, kv_properties, True)


def set_or_delete_item_properties(base_url, credentials, kv_properties, set_flag):
    """Function to set or delete properties of the artifact.
       :param base_url: The artifact URL
       :param credentials: The username:password to access Artifactory account
       :param kv_properties: The properties to set/delete on/from the Artifact
       :param set_flag: Flag indicating if the properties must be set (True) or deleted (False).
       :type base_url: String
       :type credential: String
       :type kv_properties: Dictionary
       :type set_flag: Boolean
    """
    url = base_url + "?properties="
    cred = credentials.split(':')

    try:
        if set_flag:
            url += stringify_properties_set(kv_properties)
            LOG.info('Properties set on URL: ' + url)
            req = requests.put(url, auth=HTTPBasicAuth(cred[0], cred[1]))
        else:
            url += stringify_properties_delete(kv_properties)
            LOG.info('Properties deleted on URL: ' + url)
            req = requests.delete(url, auth=HTTPBasicAuth(cred[0], cred[1]))

        # Consider any status other than 2xx an error
        if not req.status_code // 100 == 2:
            raise VantamException("Error: Unexpected response {}".format(req))
    except requests.exceptions.RequestException as exception:
        print "Error: {}".format(exception)
        raise VantamException(exception)

    return


def find_properties_uniqueness(repo, group, artifact, credentials, kv_properties, path):
    """Function to find if the properties exist on the artifact, if not an empty uris array is returned
       :param repo: The Artifactory repository
       :param group: The Artifactory group
       :param artifact: The artifact name
       :param credentials: The username:password to access Artifactory account
       :param kv_properties: The properties to find uniqueness on the Artifact
       :type repo: String
       :type group: String
       :type version: String
       :type artifact: String
       :type credential: String
       :type kv_properties: Dictionary
       :returns Array of URIs or empty array
    """
    cred = credentials.split(':')

    if "libs-" in repo:
        f_type = "folder"
        f_path = group + '/' + artifact
    else:
        f_type = "file"
        f_path = group

    payload = """items.find(
                   {"$and": [
                   {"type":{"$eq":\"""" + f_type + """\"}},
                   {"repo":{"$eq":\"""" + repo + """\"}},
                   {"path":{"$match":\"""" + f_path + """\"}},
                   """
    payload += stringify_properties_aql(kv_properties) + ']})'

    try:
        req = requests.post(path.get_base_path_aql(), data=payload, auth=HTTPBasicAuth(cred[0], cred[1]))
        # Consider any status other than 2xx an error
        if not req.status_code // 100 == 2:
            raise VantamException("Error: Unexpected response {}".format(req))
        else:
            api_uris = []
            data = json.loads(req.text)

            for entry in data['results']:
                api_uris.append(entry['repo'] + "/" + entry['path'] + "/" + entry['name'])

            return api_uris

    except requests.exceptions.RequestException as exception:
        print "Error: {}".format(exception)
        raise VantamException(exception)


def main():
    """ Module entry point """
    args = module_arg_parse()
    path = ArtifactoryPath(args.url)

    json_tag_file = open(args.params)
    data = json.load(json_tag_file)
    validate_json_file_content(data)

    # Strip following chars from the string: '[' ']' '"'
    my_string = args.kvProperties
    properties = dict(u.split(":") for u in my_string.replace("\"", "").replace(" ", "").strip('[]').split(","))

    for artifact in data["artifacts"]:
        tag_artifact(data["repo"], data["group"], args.version, artifact, args.credentials, properties, args.unique, path)

if __name__ == '__main__':
    main()
